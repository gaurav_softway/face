//
//  SWDriverDetailCell.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDriverDetailCell.h"

@implementation SWDriverDetailCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setData:(id)data{
    [self.lblPersonName setText:[data[@"name"] componentsSeparatedByString:@" "][0]];
    [self.lblPersonDetail setText:[data[@"name"] componentsSeparatedByString:@" "][1]];
    [self.lblV setText:data[@"Vehicle"]];
    NSString *str = [NSString stringWithFormat:@"%li",(long)[data[@"License_Plate"] integerValue]];

    [self.lblR setText:str];
    
}


@end
