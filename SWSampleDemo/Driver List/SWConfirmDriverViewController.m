//
//  SWDriverListViewController.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWConfirmDriverViewController.h"
#import "SWDriverDetailCell.h"
#import "SWMessageCell.h"

@interface SWConfirmDriverViewController ()<UITableViewDataSource, UITableViewDelegate>{
    BOOL messageSent;
    NSString *strDate;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (nonatomic, strong)NSArray *arrParents;
@property (nonatomic, strong)NSMutableArray *arrSelection;
@property (nonatomic, readonly)NSInteger selectionCount;


- (IBAction)handleConfirmButton:(id)sender;


@end

@implementation SWConfirmDriverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [DummyServices parentsListForUser:@"dummy"
                             complete:^(BOOL b,  NSArray *result) {
                                 self.arrParents = [NSArray arrayWithArray:result];
                                 self.arrSelection = [[NSMutableArray alloc]initWithCapacity:result.count];
                                 [self.tableView reloadData];
                             }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger i =0;
    i = strDate?1:0;
    return section==0?i:self.arrParents.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section ==1) {
        SWDriverDetailCell *header = [tableView dequeueReusableCellWithIdentifier:@"SWDriverDetailCell"];
        [header setData:self.driverDetail];
        [header.imgPerson setImage:self.image];
        return header;
    }
    return nil;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.section == 0 && strDate) {
        SWMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWMessageCell"];
        [cell setDate:strDate];
        return cell;
    }

    SWHomePersonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SWHomePersonCell" forIndexPath:indexPath];
    [cell setData:self.arrParents[indexPath.row]];

    BOOL selected = [self checkSelection:self.arrParents[indexPath.row]];
    [cell selectRow:selected];
    
    return cell;
}

#pragma mark - TableViewDlegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.f;
    }

    return 145.f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 ) {
        return 69.f;
    }

    return 77.f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    BOOL selected = [self checkSelection:self.arrParents[indexPath.row]];
    if (selected) {
        [self.arrSelection removeObject:self.arrParents[indexPath.row][@"_id"]];
    }else{
        [self.arrSelection addObject:self.arrParents[indexPath.row][@"_id"]];
    }

    self.btnConfirm.enabled = (self.selectionCount > 0);
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Config Methods
-(BOOL)checkSelection:(NSDictionary *)object{
    return  ([self.arrSelection containsObject:object[@"_id"]]) ;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Config Methods
-(NSInteger)selectionCount{
    NSLog(@"%@", @(self.arrSelection.count));
    return self.arrSelection.count;
}

-(void)applySignals{
    
}
#pragma mark - Action Methods

- (IBAction)handleConfirmButton:(UIButton *)sender {
    
    if ([sender.titleLabel.text isEqualToString:@"Done"]) {
        UIViewController *vc = self.navigationController.viewControllers [1];
        [self.navigationController popToViewController:vc animated:YES];

        return;
    }
    if (self.selectionCount ==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No parents selected." message:@"Please select atleast one parent to send them message." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;

        
        return;
    }
    NSDate *date = [NSDate date];
    strDate = [self stringWithDate:date];
    [self.tableView reloadData];
    [self.tableView setAllowsSelection:NO];
    [sender setTitle:@"Done" forState:UIControlStateNormal];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    
}
- (NSString *)stringWithDate:(NSDate *)date
{
    return [NSDateFormatter localizedStringFromDate:date
                                          dateStyle:NSDateFormatterMediumStyle
                                          timeStyle:NSDateFormatterMediumStyle];
}

-(void)reloadData{
    [self.tableView reloadData];
}
@end
