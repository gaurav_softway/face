//
//  SWDriverDetailCell.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomePersonCell.h"

@interface SWDriverDetailCell : SWHomePersonCell
@property (weak, nonatomic) IBOutlet UILabel *lblV;
@property (weak, nonatomic) IBOutlet UILabel *lblR;

@end
