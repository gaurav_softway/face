//
//  SWVerificationCameraVC.h
//  SWFaceDemo
//
//  Created by Hemanth on 05/05/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDetectionCameraVC.h"
@interface SWVerificationCameraVC : SWDetectionCameraVC<FaceCaptureSampleDelegate>
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (strong, nonatomic)  UIImage *capturedImage;
-(void)pushMatchVCWithDerivedData:(id)derivedData;
@end
