//
//  SWVerificationCameraVC.m
//  SWFaceDemo
//
//  Created by Hemanth on 05/05/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWVerificationCameraVC.h"
#import "SWPickUpViewController.h"
#import "SWConfirmDriverViewController.h"


@implementation SWVerificationCameraVC

- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_FACE;
        // standard alloc/init with frame call for a UIView subclass
        self.mobbIDView = [[FaceView alloc] initWithFrame:self.cameraView.frame];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        // add the sdk view as a subview to the view controllers view
        [self.mobbIDView setCaptureDelegate:self];
        [self.view addSubview:self.mobbIDView];
    }
}
- (void)faceSampleCaptured:(UIImage *)face
{
    self.capturedImage = face;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self createView];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"Start user recognition process");
    
    [self.mobbIDView startRecognitionIn:self.recognitionMode forUser:self.userId];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)identificationFinishedForUsers:(NSArray *)users
                                result:(MobbIDSDKOperationIdentificationResult)resultCode
                                method:(MobbIDSDKBiometricMethod)method
                                 error:(NSError *)errorOccurred
{
    if (resultCode == MobbIDSDKOperationIdentificationResult_USER_IDENTIFIED) {
        if (users.count>0) {
            [self.delegate mobbIDViewIdentificationFinishedWithResult:resultCode method:method withUserID:[users firstObject][@"user"] withSelf:self error:errorOccurred];
            
            return;
        }
    }
    [self pushUnMatchVC];
}
-(void)pushMatchVCWithDerivedData:(id)derivedData
{
    SWConfirmDriverViewController *cvc =[self.storyboard instantiateViewControllerWithIdentifier:@"SWConfirmDriverViewController"];
    [cvc setDriverDetail:derivedData];
    [cvc setImage:self.capturedImage];
    [self.navigationController pushViewController:cvc animated:YES];
}
-(void)pushUnMatchVC
{
    SWPickUpViewController *pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWPickUpViewController"];
    [pvc setInputImage:self.capturedImage];
    [self.navigationController pushViewController:pvc animated:YES];
}
@end
