//
//  SWDetectionCameraVC.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FJFaceDetector.h"
#import "SWBaseViewController.h"

@interface SWDetectionCameraVC : SWBaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) FJFaceDetector *faceDetector;
@property (strong, nonatomic) NSDictionary *driverData;
@end
