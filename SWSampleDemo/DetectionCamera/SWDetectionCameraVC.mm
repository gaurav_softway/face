//
//  SWDetectionCameraVC.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDetectionCameraVC.h"
#import "SWFaceMatcher.h"
#import "SWPickUpViewController.h"
#import "SWConfirmDriverViewController.h"


@interface SWDetectionCameraVC ()<FJFaceDetectorDelegate>{
    BOOL cameraStateActive;
}
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
- (IBAction)handleCaptureButton:(id)sender;
- (IBAction)handleFlipButton:(id)sender;
- (IBAction)handleFlash:(UIButton *)sender;
@end

@implementation SWDetectionCameraVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imgView setContentMode:UIViewContentModeScaleAspectFit];
    
    self.faceDetector = [[FJFaceDetector alloc] initWithCameraView:_imgView scale:2.0];
    [self.faceDetector setDelegate:self];

}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.imgView.image = nil;
    if (!cameraStateActive) {
        cameraStateActive =YES;
        [self.faceDetector startCapture];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (cameraStateActive) {
        cameraStateActive =NO;
        [self.faceDetector stopCapture];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"nomatch"]) {
        SWPickUpViewController *pvc = segue.destinationViewController;
        [pvc setDriverData:sender];
        [pvc setInputImage:self.imgView.image];
    }else if([segue.identifier isEqual:@"match"]) {
        SWConfirmDriverViewController *cvc = segue.destinationViewController;
        [cvc setDriverDetail:sender];
        [cvc setImage:self.imgView.image];
    }
}


- (IBAction)handleCaptureButton:(id)sender {
   __unused NSArray *detectedFaces = [self.faceDetector.detectedFaces copy];

    BOOL foundRect = [detectedFaces firstObject] !=nil;
    if (!foundRect)return;
    UIImage *detectedImage = [self.faceDetector faceWithIndex:0];
    [self.faceDetector stopCapture];
    [self.imgView setImage:detectedImage];
    
    [SWFaceMatcher searchNameForImage:detectedImage withResult:^(BOOL match, id result) {
        if (!match) {
            [self performSegueWithIdentifier:@"nomatch" sender:self.driverData];
        }else{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name LIKE[cd] %@", result];

            [DummyServices driversListForUser:@"user1" school:@"school1" complete:^(BOOL b, NSArray *r)  {
                if (b) {
                    NSArray *arr = [r filteredArrayUsingPredicate:predicate];
                    if (arr.count>0) {
                        [self performSegueWithIdentifier:@"match" sender:[arr firstObject]];
                        return;
                    }
                }
                [self performSegueWithIdentifier:@"nomatch" sender:self.driverData];

            }];
        }
    }];
}
- (IBAction)handleFlash:(UIButton *)sender {
    NSString *imgName = nil;
    
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (device.torchMode == AVCaptureTorchModeOff)
            {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                imgName = @"flash-off";
                
            }
            else
            {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                                imgName = @"flash-on";
                
            }
            [device unlockForConfiguration];
        }
    }else{
        imgName = @"flash-on";
    }
    [sender setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];

}
- (IBAction)handleFlipButton:(UIButton *)sender {
//    frontCameraActive = !frontCameraActive ;
//    NSLog(@"%@", frontCameraActive?@"Front Camera": @"Rear Camera");
    [self.faceDetector.videoCamera switchCameras];
}

#pragma mark - FJDElegate
-(void)cameraLoaded{
    [self.lblMessage setHidden:YES];
}
@end
