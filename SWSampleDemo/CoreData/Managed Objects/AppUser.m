//
//  AppUser.m
//  SWFaceDemo
//
//  Created by Hemanth on 06/05/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "AppUser.h"


@implementation AppUser

@dynamic about;
@dynamic address;
@dynamic age;
@dynamic authID;
@dynamic company;
@dynamic eyeColor;
@dynamic gender;
@dynamic guid;
@dynamic imagePath;
@dynamic index;
@dynamic isActive;
@dynamic latitude;
@dynamic license_Plate;
@dynamic longitude;
@dynamic name;
@dynamic phone;
@dynamic picture;
@dynamic registered;
@dynamic userID;
@dynamic vehicle;
@dynamic language;

@end
