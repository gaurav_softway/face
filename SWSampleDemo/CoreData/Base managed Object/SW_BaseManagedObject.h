//
//  SW_BaseManagedObject.h
//  
//
//  Created by Gaurav Keshre on 6/15/14.
//  Copyright (c) 2014 Softway Solutions. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface SW_BaseManagedObject : NSManagedObject

@property(nonatomic)BOOL traversed;
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
+ (NSString*)entityName;
- (NSDictionary*) toDictionary;
@end
