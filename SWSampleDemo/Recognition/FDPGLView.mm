//
//  FDPGLView.m
//  GKFaceIdentifyDEmo
//
//  Created by Gaurav Keshre on 4/2/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import "FDPGLView.h"

@implementation FDPGLView

// Override the class method to return the OpenGL layer, as opposed to the normal CALayer
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

- (void)dealloc
{
    context = NULL;
    [self destroyFramebuffer];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
        // Do OpenGL Core Animation layer setup
        CAEAGLLayer * eaglLayer = (CAEAGLLayer *)self.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        if (!context || ![EAGLContext setCurrentContext:context] || ![self createFramebuffers]) {
        }
}

#pragma mark -
#pragma mark OpenGL drawing

- (BOOL)createFramebuffers
{
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    
    // Onscreen framebuffer object
    glGenFramebuffers(1, &viewFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
    
    glGenRenderbuffers(1, &viewRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
    
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
#if defined(DEBUG)
    NSLog(@"Backing width: %d, height: %d", backingWidth, backingHeight);
#endif
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, viewRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
#if defined(DEBUG)
        NSLog(@"Failure with framebuffer generation");
#endif
        return NO;
    }
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
#if defined(DEBUG)
        NSLog(@"Incomplete FBO: %d", status);
#endif
        exit(1);
    }
    
    return YES;
}

- (void)destroyFramebuffer;
{
    if (viewFramebuffer) {
        glDeleteFramebuffers(1, &viewFramebuffer);
        viewFramebuffer = 0;
    }
    if (viewRenderbuffer) {
        glDeleteRenderbuffers(1, &viewRenderbuffer);
        viewRenderbuffer = 0;
    }
}

- (void)setDisplayFramebuffer;
{
    if (context) {
        //[EAGLContext setCurrentContext:context];
        if (!viewFramebuffer) {
            [self createFramebuffers];
        }
        glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
        glViewport(0, 0, backingWidth, backingHeight);
    }
}

- (BOOL)presentFramebuffer;
{
    BOOL success = FALSE;
    if (context) {
        //[EAGLContext setCurrentContext:context];
        glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
        success = [context presentRenderbuffer:GL_RENDERBUFFER];
    }
    return success;
}

- (UIImage *)drawGlToImage
{
    // Draw OpenGL data to an image context
    
    UIGraphicsBeginImageContext(self.frame.size);
    
    unsigned char buffer[320 * 480 * 4];
    
    CGContextRef aContext = UIGraphicsGetCurrentContext();
    
    glReadPixels(0, 0, 320, 480, GL_RGBA, GL_UNSIGNED_BYTE, &buffer);
    
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, &buffer, 320 * 480 * 4, NULL);
    
    CGImageRef iref = CGImageCreate(320,480,8,32,320*4, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaLast, ref, NULL, true, kCGRenderingIntentDefault);
    
    CGContextScaleCTM(aContext, 1.0, -1.0);
    CGContextTranslateCTM(aContext, 0, -self.frame.size.height);
    
    UIImage *im = [[UIImage alloc] initWithCGImage:iref];
    
    UIGraphicsEndImageContext();
    
    return im;
}
@end
