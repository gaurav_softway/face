//
//  FDPCamera.h
//  GKFaceIdentifyDEmo
//
//  Created by Gaurav Keshre on 4/1/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>

@protocol RecognitionCameraDelegate;

@interface FDPCamera : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate>
{
    AVCaptureVideoPreviewLayer *videoPreviewLayer;
    AVCaptureSession *captureSession;
    AVCaptureDeviceInput *videoInput;
    AVCaptureVideoDataOutput *videoOutput;
}

@property(nonatomic, weak) id<RecognitionCameraDelegate> delegate;
@property(readonly) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@end

@protocol RecognitionCameraDelegate
- (void)cameraHasConnected;
- (void)processNewCameraFrame:(CVImageBufferRef)cameraFrame;


@end
