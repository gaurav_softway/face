//
//  SWLoginViewController.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWLoginViewController.h"

@interface SWLoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;

-(IBAction)handleLoginButton:(id)sender;

@end

@implementation SWLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods
-(IBAction)handleLoginButton:(id)sender{
    if ([self.txtPass.text length]<1 ||[self.txtUsername.text length]<1) {
        return;
    }
    
//    [self.btnSignIn setEnabled:NO];
    [DummyServices signInWithUsername:self.txtUsername.text password:self.txtPass.text
                             complete:^(BOOL b, id result/*always nil here*/) {
//                                 [self.btnSignIn setEnabled:YES];
                                 if (b) {
                                     [self performSegueWithIdentifier:@"sLoginSuccess" sender:nil];
                                 }else{
                                     
                                 }
                             }];

    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:@"add"]){
     
        
        
        return;
    }
}



@end
