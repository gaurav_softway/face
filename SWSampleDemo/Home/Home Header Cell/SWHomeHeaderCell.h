//
//  SWHomeHeaderCell.h
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomeHeaderCell : UITableViewCell<SWDataReceiverProtocol>
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end
