//
//  SWHomePersonCell.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomePersonCell.h"
@implementation SWHomePersonCell

- (void)awakeFromNib {
    // Initialization code
 }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)selectRow:(BOOL)selected{

    [self.contentView setBackgroundColor:selected?[UIColor colorWithRed:0.955 green:0.961 blue:0.675 alpha:1.000]:[UIColor whiteColor]];
}

#pragma mark - FDPDataReceiverProtocol Methods
-(void)setDate:(NSString *)date{
    [self.lblPersonName setText:@"Message sent to parents"];
    [self.lblPersonDetail setText:date];
}
-(void)setData:(id)data{
    [self.lblPersonName setText:data[@"name"]];
    [self.lblPersonDetail setText:data[@"company"]];

}

@end
