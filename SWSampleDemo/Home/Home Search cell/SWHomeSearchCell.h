//
//  SWHomeSearchCell.h
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomeSearchCell : UITableViewCell<SWDataReceiverProtocol>
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;

@end
