//
//  SWAppearanceManager.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/6/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SWAppearanceManager.h"
#import "AppDelegate.h"

@implementation SWAppearanceManager
+(void)applyTheme{
    /*
     * Appearance
     */
    
    NSShadow* shadow = [NSShadow new];
    shadow.shadowOffset = CGSizeMake(0.0f, 0.0f);
    shadow.shadowColor = [UIColor colorWithRed:0.918 green:0.943 blue:0.929 alpha:1.000];
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [UIColor colorWithRed:0.994 green:0.962 blue:1.000 alpha:1.000],
                                                            NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:20.0f],
                                                            NSShadowAttributeName: shadow
                                                            }];
}

+(void)addStatusBarWithColor:(UIColor *)color{
    /*
     * Status bar
     */
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    CGRect frame = delegate.window.bounds;
    frame.size.height = 20;
    UIView *v = [[UIView alloc]initWithFrame:frame];
    [v setBackgroundColor:color];
    [delegate.window.rootViewController.view addSubview:v];
    
        UINavigationController *root = (UINavigationController *) delegate.window.rootViewController;
    
    [root.view insertSubview:v belowSubview:root.navigationBar];
    
    v=nil;

}
@end
