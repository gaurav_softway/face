//
//  UIColor+AppTheme.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/6/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppTheme)
+(UIColor *)appBlueColor;
+(UIColor *)appGreenColor;
@end
