//
//  DummyServices.m
//  FaceDetectionPOC
//
//  Created by Gaurav Keshre on 3/31/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import "DummyServices.h"

@implementation DummyServices

#pragma mark - COMMON Methods

+ (void)delayServiceCall:(double)delayInSeconds onComplete:(DummyCommonBlock)completeBloc{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), completeBloc);
}

+(void)parsedDataFor:(NSString *)jsonFilename onComplete:(DummyServiceResponse)completeBlock{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSString *filePath = [[NSBundle mainBundle]
                              pathForResource:jsonFilename
                              ofType:@"json"];

        NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        NSError *error =  nil;

        NSArray *jsonDataArray = [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completeBlock(YES,jsonDataArray);
            
        });
    });

}

#pragma mark - LOGIN Methods
+ (void)signInWithUsername:(NSString *)username password:(NSString *)password complete:(DummyServiceResponse)completeBlock {
    if (username ==nil || password ==nil ) {
        completeBlock(NO, @"Incomplete paramenters.");
        return;
    }
    [DummyServices delayServiceCall:0 onComplete:^{
        BOOL success = [username isEqualToString:@"user"] && [password isEqualToString:@"pass"];
        completeBlock(success, nil);
    }];
}

#pragma mark - Drivers Methods
+ (void)driversListForUser:(NSString *)userID
                    school:(NSString *)schoolID
                  complete:(DummyServiceResponse)completeBlock{
    if (userID) {
     [DummyServices parsedDataFor:JSON_Drivers
                       onComplete:completeBlock];
        return;
    }
       completeBlock(NO, @"Invalid User ID");
    
}

+ (void)parentsListForUser:(NSString *)userID
                  complete:(DummyServiceResponse)completeBlock{
    if (userID) {
        [DummyServices parsedDataFor:JSON_Parents
                          onComplete:completeBlock];
        return;
    }
    completeBlock(NO, @"Invalid User ID");
    
}

@end
