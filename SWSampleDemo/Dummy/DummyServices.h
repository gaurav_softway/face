//
//  DummyServices.h
//  FaceDetectionPOC
//
//  Created by Gaurav Keshre on 3/31/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSON_Drivers @"drivers"
#define JSON_Parents @"parents"
#define JSON @"json"

typedef void (^DummyServiceResponse)(BOOL, id);
typedef void (^DummyCommonBlock)(void);

@interface DummyServices : NSObject
+ (void)signInWithUsername:(NSString *)username password:(NSString *)password complete:(DummyServiceResponse)completeBlock;

+ (void)driversListForUser:(NSString *)userID
                  school:(NSString *)schoolID
                  complete:(DummyServiceResponse)completeBlock;

+ (void)parentsListForUser:(NSString *)userID
                  complete:(DummyServiceResponse)completeBlock;

@end
