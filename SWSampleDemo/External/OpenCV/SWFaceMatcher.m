//
//  SWFaceMatcher.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWFaceMatcher.h"

@implementation SWFaceMatcher


+(instancetype)sharedInstance{
    static SWFaceMatcher *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SWFaceMatcher alloc]init];
    });
    return instance;
}

-(instancetype)init{
    self = [super init];
    NSURL *modelURL = [self faceModelFileURL];
    self.faceModel = [FJFaceRecognizer faceRecognizerWithFile:[modelURL path]];
    
    return self;
}
+(BOOL)checkImage:(UIImage *)_inputImage against:(NSString *)userID{
    return [[SWFaceMatcher sharedInstance]checkImage:_inputImage
                                             against:userID];
}

-(BOOL)checkImage:(UIImage *)_inputImage
          against:(NSString *)userID{
    double confidence;
    
    if (_faceModel.labels.count == 0) {
        return NO;
    }
    
    NSString *name = [_faceModel predict:_inputImage confidence:&confidence];
    return (name && [name isEqualToString:userID]);
}

+(void)matchAndUpdateImage:(UIImage *)image
                       for:(NSString *)strID
              onCompletion:(SWGeneralBlock)block{
    [[SWFaceMatcher sharedInstance]matchAndUpdateImage:image
                                                   for:strID
                                          onCOmpletion:block];
}

-(void)matchAndUpdateImage:(UIImage *)_inputImage for:(NSString *)strID onCOmpletion:(SWGeneralBlock)block;
{
    [_faceModel updateWithFace:_inputImage name:strID];
    [_faceModel serializeFaceRecognizerParamatersToFile:[[self faceModelFileURL] path]];
    if (block) {
        block();
    }
}

+(void)searchNameForImage:(UIImage *)_inputImage withResult:(SWResultBlock)block{
    [[SWFaceMatcher sharedInstance]searchNameForImage:_inputImage withResult:block];
}

-(void)searchNameForImage:(UIImage *)_inputImage withResult:(SWResultBlock)block{
    if (_faceModel.labels.count == 0) {
        block(NO, @"No faces in database");
    }

    double confidence;
    NSString *name = [_faceModel predict:_inputImage confidence:&confidence];
    if (block) {
        block(name!=nil, name);
    }
}

#pragma mark - Helper Methods
- (NSURL *)faceModelFileURL {
    NSArray *paths = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentsURL = [paths lastObject];
    NSURL *modelURL = [documentsURL URLByAppendingPathComponent:@"face-model.xml"];
    return modelURL;
}

@end
