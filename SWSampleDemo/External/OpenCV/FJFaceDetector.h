//
//  CVCameraProvider.h
//  opencvtest
//
//  Created by Engin Kurutepe on 16/01/15.
//  Copyright (c) 2015 Fifteen Jugglers Software. All rights reserved.
//

#import <opencv2/highgui/cap_ios.h>
#import <Foundation/Foundation.h>

@protocol FJFaceDetectorDelegate<NSObject>
-(void)cameraLoaded;
@end


@interface FJFaceDetector : NSObject <CvVideoCameraDelegate>
@property(nonatomic, weak)id <FJFaceDetectorDelegate> delegate;
@property (nonatomic, strong) CvVideoCamera* videoCamera;

- (instancetype)initWithCameraView:(UIImageView *)view scale:(CGFloat)scale;

- (void)startCapture;
- (void)stopCapture;

- (NSArray *)detectedFaces;
- (UIImage *)faceWithIndex:(NSInteger)idx;
@end
