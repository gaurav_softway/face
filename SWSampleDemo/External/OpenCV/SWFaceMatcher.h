//
//  SWFaceMatcher.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FJFaceRecognizer.h"

typedef void(^SWGeneralBlock)(void);
typedef void(^SWResultBlock)(BOOL match, id result);

@interface SWFaceMatcher : NSObject
@property (nonatomic, strong) FJFaceRecognizer *faceModel;

+(instancetype)sharedInstance;

+(BOOL)checkImage:(UIImage *)_inputImage
          against:(NSString *)userID;

+(void)searchNameForImage:(UIImage *)_inputImage withResult:(SWResultBlock)block;

+(void)matchAndUpdateImage:(UIImage *)image
          for:(NSString *)strID
              onCompletion:(SWGeneralBlock)block;
@end
