//
//  SWAddCameraVC.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWEnrollFaceVC.h"
#import "EAColourfulProgressView.h"
@interface SWEnrollFaceVC()
{
    __weak UIView *rectFrameView;
    __weak IBOutlet EAColourfulProgressView *progressBar;
}
@end

@implementation SWEnrollFaceVC
- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_FACE;
        // standard alloc/init with frame call for a UIView subclass
        self.mobbIDView = [[FaceView alloc] initWithFrame:self.cameraView.frame];
        [(FaceView *)self.mobbIDView setTintColor:[UIColor yellowColor]];
        [self.mobbIDView setFaceDetectionDelegate:self];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        [self.mobbIDView setCaptureDelegate:self];
        [self.mobbIDView setProgressUpdateDelegate:self];
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
    [progressBar updateToCurrentValue:0 animated:NO];

}
- (void)faceSampleCaptured:(UIImage *)face
{
    self.capturImage = face;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [self createView];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"Start user recognition process");
    
    [self.mobbIDView startRecognitionIn:self.recognitionMode forUser:self.userId];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleCapture:(id)sender {
   
    //    self.lblText.text = @"Please Wait...";
}

#pragma mark - FaceDetectionDelegate Methods
- (void)onFaceDetected:(CGRect)faceRect distanceOk:(BOOL)distanceOk{
    if (!distanceOk) {
        self.lblText.text = @"Get closer to camera...";
        [self.lblText.superview bringSubviewToFront:self.lblText];
        return;
    }else {
        self.lblText.text = @"";
    }
    
    [[self rectView] setFrame:faceRect];
    UIColor *bColor = distanceOk?[UIColor greenColor]:[UIColor redColor];
    [self rectView].layer.borderColor = bColor.CGColor;
}

/**
 This methods gets called each time a face is not detected in the image.
 */
- (void)onNoFaceDetected{
    [rectFrameView removeFromSuperview];
    rectFrameView = nil;
}

-(UIView *)rectView{
    if (rectFrameView == nil) {
        UIView *_rectFrameView = [[UIView alloc]init];
        [_rectFrameView setBackgroundColor:[UIColor clearColor]];
        _rectFrameView.layer.borderWidth = 3.f;
        _rectFrameView.layer.borderColor = [UIColor yellowColor].CGColor;
        [self.view addSubview:_rectFrameView];
        rectFrameView = _rectFrameView;
        _rectFrameView = nil;
    }
    return rectFrameView;

}
#pragma mark - MobbIDSDKProgressUpdateDelegate Methods

- (void)progressUpdateWithProgressOK:(float)progressOK progressKO:(float)progressKO{
        NSLog(@"progressOK: %f progressKO: %f", progressOK, progressKO);
    [progressBar updateToCurrentValue:(progressBar.maximumValue - progressOK*10) animated:YES];
}

#pragma mark - Override Methods
-(void)verificationFinished:(MobbIDSDKOperationVerificationResult)resultCode data:(MobbIDSDKOperationVerificationResultData *)data error:(NSError *)errorOccurred{
    [super verificationFinished:resultCode data:data error:errorOccurred];
    
//    [self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];

}

- (void) enrollmentFinished:(MobbIDSDKOperationEnrollmentResult)resultCode
                       data:(MobbIDSDKOperationEnrollmentResultData *)data
                      error:(NSError *)errorOccurred {

    [super enrollmentFinished:resultCode
                         data:data
                        error:errorOccurred];

//    [self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

@end
