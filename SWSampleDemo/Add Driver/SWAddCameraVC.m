//
//  SWAddCameraVC.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWAddCameraVC.h"
#import "FJFaceDetector.h"
#import "SWFaceMatcher.h"


@interface SWAddCameraVC ()<UIAlertViewDelegate>
@property (nonatomic, strong) FJFaceDetector *faceDetector;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation SWAddCameraVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                 initWithTarget:self
                                 action:@selector(handleTap:)];
    
    [self.view addGestureRecognizer:_tapGestureRecognizer];
    self.view.userInteractionEnabled = YES;
    self.title = self.driverData[@"name"];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.faceDetector startCapture];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.faceDetector stopCapture];
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    NSArray *detectedFaces = [self.faceDetector.detectedFaces copy];
    CGSize windowSize = self.view.bounds.size;
    for (NSValue *val in detectedFaces) {
        CGRect faceRect = [val CGRectValue];
        
        CGPoint tapPoint = [tapGesture locationInView:nil];
        //scale tap point to 0.0 to 1.0
        CGPoint scaledPoint = CGPointMake(tapPoint.x/windowSize.width, tapPoint.y/windowSize.height);
        if(CGRectContainsPoint(faceRect, scaledPoint)){
            NSLog(@"tapped on face: %@", NSStringFromCGRect(faceRect));
            UIImage *img = [self.faceDetector faceWithIndex:[detectedFaces indexOfObject:val]];
            
            [SWFaceMatcher matchAndUpdateImage:img
                                           for:self.driverData[@"name"]
                                  onCompletion:^{
                                      UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Saved" message:@"Face saved successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                                      [alert show];
                                      alert = nil;
                                  }];
        }
        else {
            NSLog(@"tapped on no face");
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleCapture:(id)sender {
    
    
}

#pragma mark - UIAlertViewDelegate Methods
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
