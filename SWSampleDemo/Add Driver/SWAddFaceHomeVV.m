//
//  SWAddFaceHomeVV.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWAddFaceHomeVV.h"
#import "SWAddCameraVC.h"

@interface SWAddFaceHomeVV ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation SWAddFaceHomeVV

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview Methods


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     [self performSegueWithIdentifier:@"addcamera" sender:arrDrivers[indexPath.row]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SWAddCameraVC *cvc = segue.destinationViewController;
    [cvc setDriverData:sender];
}

@end
