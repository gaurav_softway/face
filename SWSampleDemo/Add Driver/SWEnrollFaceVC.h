//
//  SWAddCameraVC.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDetectionCameraVC.h"

@interface SWEnrollFaceVC : SWDetectionCameraVC<FaceCaptureSampleDelegate, FaceDetectionDelegate, MobbIDSDKProgressUpdateDelegate>

@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) UIImage *capturImage;

@end
