//
//  PogoPenManager.h
//  SignatureCaptureLib-iOS
//
//  Created by Rodrigo Sánchez González on 18/02/14.
//  Copyright (c) 2014 Mobbeel. All rights reserved.
//

#import "ActivePenManager.h"

@interface PogoPenManager : ActivePenManager

@end
