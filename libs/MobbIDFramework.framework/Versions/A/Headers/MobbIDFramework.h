//
//  MobbIDFramework.h
//  MobbIDFramework
//
//  Created by Abraham Holgado on 16/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

/// Configuration Singleton for the SDK.
#import <MobbIDFramework/MobbIDAPI.h>

/// User's management API
#import <MobbIDFramework/MobbIDManagementAPI.h>

/// Common types for the Framework
#import <MobbIDFramework/MobbIDSDKTypes.h>

/// Biometric delegate that should be adopted by any class performing biometric recognition.
#import <MobbIDFramework/BiometricMethodDelegate.h>

/// Biometric delegate that should be adopted by any class performing face biometric recognition.
#import <MobbIDFramework/IdentificationDelegate.h>

// Delegate to be adopted by any class to receive the synchronization response
#import <MobbIDFramework/DownloadTemplateDelegate.h>

/// Biometric views.
#import <MobbIDFramework/MobbIDSDKBaseView.h>
#import <MobbIDFramework/FaceView.h>
#import <MobbIDFramework/VoiceView.h>
#import <MobbIDFramework/SignatureView.h>
#import <MobbIDFramework/IrisView.h>
#import <MobbIDFramework/FaceVoiceView.h>

#import "SignatureAPI.h"
#import "FaceAPI.h"
#import "VoiceAPI.h"
#import "IrisAPI.h"
#import "FaceVoiceAPI.h"
