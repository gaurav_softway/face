//
//  SignatureData.h
//  BCSignatureC
//
//  Created by Abraham Holgado on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The SignatureData class represents a specific instant on the signature acquisition process.
 */
@interface SignatureData : NSObject<NSCoding> {
    long long int _time;
    float _x;
    float _y;
    float _pressure;
}

/**
 * Time offset of point in ms.
 */
@property (nonatomic) long long int time;

/**
 * X position.
 */
@property (nonatomic) float x;

/**
 * Y position.
 */
@property (nonatomic) float y;

/**
 * Pen force (pressure).
 */
@property (nonatomic) float pressure;

/** @name Constructors */

/**
 Convenience method for initializing a SignatureData.
 @param x The X coordinate of the point.
 @param y The Y coordinate of the point.
 @param p The pressure or simulated pressure of the point.
 @param time The time offset of the point in ms.
 */
- (id) initWith:(float)x Y:(float)y P:(float)p Time:(long long int)time;

@end
