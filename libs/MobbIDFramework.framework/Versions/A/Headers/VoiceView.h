//
//  VoiceView.h
//  MobbIDAPI
//
//  Created by Raul Jareño diaz on 03/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "MobbIDSDKTypes.h"
#import "MobbIDSDKProgressUpdateDelegate.h"

/**
 The delegate of a Voice capture sample View (VoiceView) must adopt this protocol to receive the samples of the biometric view. 
 */
@protocol VoiceCaptureSampleDelegate <NSObject>

@required

/**
 This method gets called each time a sample voice is capture.
 
 If the resultCode is `MobbIDSDKOperationCaptureResult_DATA_CAPTURED` then the voice sample will be avaiblable in the `voice` parameter.
 
 @param resultCode MobbIDSDKOperationCaptureResult The result of the capture operation.
 @param voice NSData NSData containg the user's voice sample (encoded using .wav format) if the result of the operation is `MobbIDSDKOperationCaptureResult_DATA_CAPTURED`.
 @param errorOccurred NSError If an error has occurred when capturing the voice all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_PERMISSION_DENIED
 
 */
- (void)voiceSampleCaptureFinishedWithResult:(MobbIDSDKOperationCaptureResult)resultCode
                                   voiceData:(NSData*)voice
                                       error:(NSError *)errorOccurred;

@end

/**
 VoiceView is the Biometric Recognition View that should be used to enroll (register) or verify a user using his/her voice as the biometric method.
 
 Before performing any biometric operation (enrollment or verification) please ensure that:
 
 - The user exists (@see MobbIDManagementAPI for more information)
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the VoiceView to enroll an existing user you must follow these steps:
 
 1) Create and init VoiceView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `VoiceView *voiceView = [[VoiceView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[voiceView setDelegate:self];`
 
 3) Add the VoiceView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:voiceView];`
 
 4) Configure the type of speech mode you want to use. @see MobbIDSDKVoiceRecognitionSpeechMode for more information.
 
 `[voiceView setSpeechMode:MobbIDSDKVoiceRecognitionLanguage_RandomNumbers];`
 
 5) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[voiceView startRecognitionIn:MobbIDSDKRecognitionMode_Enrollment forUser:USERID];`
 
 6) Stop the biometric recognition process when it is finished (method enrollmentFinishedForUser:result:method: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[voiceView stopRecognition];`
 
 To use the VoiceView to verify a previously enrolled user you follow the previous steps and specify the verification mode instead of the enrollment:
 
 `// voiceView is init and added as a subview to the ViewController's view.`
 
 `[voiceView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 To use the VoiceView to capture the samples you have to set up its captureDelegate, conforming the VoiceCaptureSampleDelegate. The captureDelegate does not have to be the ViewController (it could be any class conforming the VoiceCaptureSampleDelegate).
 
 `[voiceView setCaptureDelegate:self];`
 
 If you dont want to perform the recognition you have to use the MobbIDSDKRecognitionMode_Capture in the method startRecognitionIn:forUser:
 
 `[voiceView startRecognitionIn:MobbIDSDKRecognitionMode_Capture forUser:USERID];`
 
 
 
 #Customization Options#
 
 *This customization system will be changed in the next major release (4.0) in favor of the one implemented in the SignatureView*
 
 Use these propeties to customize the aspect of the view:
 
- customBackground
- customInstructionBackgroundTop
- customInstructionBackgroundMiddle
- customInstructionBackgroundBottom
- customRecording
- customRecordingBackground
 
 */
@interface VoiceView : MobbIDSDKBaseView

/**
 This property should be set to the same language that was used in the user creation. If it is not, you could experience an unexpected behaviour.
 @warning. This property would be obsolete in the next release
 */
@property (nonatomic, assign) MobbIDAPISupportedLanguage language;

/** 
 Specifies the speech mode that will be used in the enrollment (and later verifications) 
 */
@property (nonatomic, assign) MobbIDSDKVoiceRecognitionSpeechMode speechMode;

/** 
 YES if flac encoding should be used 
 */
@property (nonatomic, assign) BOOL useFlacEncoding;

@property (nonatomic, strong) UIImage * customBackground;
@property (nonatomic, strong) UIImage * customInstructionBackgroundTop;
@property (nonatomic, strong) UIImage * customInstructionBackgroundMiddle;
@property (nonatomic, strong) UIImage * customInstructionBackgroundBottom;
@property (nonatomic, strong) UIImage * customRecording;
@property (nonatomic, strong) UIImage * customRecordingBackground;

/**
 Set this delegate if you want to get the sample information of each audio fragment captured.
 */
@property (nonatomic, weak) id<VoiceCaptureSampleDelegate> captureDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the recognition progress.
 */
@property (nonatomic, weak) id<MobbIDSDKProgressUpdateDelegate> progressUpdateDelegate;

@end
