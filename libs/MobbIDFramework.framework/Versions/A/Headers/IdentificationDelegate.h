//
//  IdentificationDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 15/04/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the doIdentification: operation.
 */
typedef NS_ENUM(NSInteger, MobbIDAPIIdentificationDelegateResult) {
    /** The user has been identified */
    MobbIDAPIIdentificationDelegateResult_USER_IDENTIFIED,
    /** The user has not been identified */
    MobbIDAPIIdentificationDelegateResult_USER_NOT_IDENTIFIED,
    /** The process needs more samples to perform the verification */
    MobbIDAPIIdentificationDelegateResult_NEED_MORE_SAMPLES,
    /** There has been some error with the operation. */
    MobbIDAPIIdentificationDelegateResult_ERROR
};

/**
 Data returned by the identification process.
 
 @since 3.4
 */
@interface MobbIDAPIIdentificationResultData : NSObject

/**
 This value contains all candidates found with the given samples when the result is MobbIDAPIIdentificationDelegateResult_USER_IDENTIFIED, in any other case it will be nil. Each user of the array will be represented by an NSDictionary with two objects:
 
 - NSString with the userId for the key kMobbIDAPI_IdentificationUserKey
 - NSNumber with the score (float) for the key kMobbIDAPI_IdentificationScoreKey
 
 */
@property (nonatomic, copy) NSArray * users;

/**
 This value will return the biometric method in which the user is trying to be verified. It could take one of this values (the constants are defined in MobbIDBaseAPI):
 
 - kMobbIDAPI_BiometricMethod_Signature;
 - kMobbIDAPI_BiometricMethod_Face;
 
 */
@property (nonatomic, copy) NSString * biometricMethod;

/** Value from 0.0 to 100.0 that determines the progress to obtain a successful result. */
@property (nonatomic, assign) CGFloat progressOK;

/** Value from 0.0 to 100.0 that determines the progress to obtain an erroneous  result. */
@property (nonatomic, assign) CGFloat progressKO;

@end

/** Key used to retrieve the user from the user's NSArray */
extern const NSString * kMobbIDAPI_IdentificationUserKey;

/** Key used to retrieve the score of the user from the user's NSArray */
extern const NSString * kMobbIDAPI_IdentificationScoreKey;

/**
 Delegate to be informed of the result of a biometric identification process.
 
 Once the biometric identification process has started, the API would always return (asynchronously) a value for every time the "doIdentification..." is invoked. Since the number of biometric samples is not fixed, the actual numbers of invocations is unknown. The usual process could be something like this:
 
 1. Start identification process (startIdentificationdelegate:)
 2. Invoke doIdentification with the acquired sample(s)
 3. Receive the result with this delegate. If the result is MobbIDAPIIdentificationDelegateResult_USER_IDENTIFIED or MobbIDAPIIdentificationDelegateResult_USER_NOT_IDENTIFIED the process has finished. If the result is MobbIDAPIIdentificationDelegateResult_NEED_MORE_SAMPLES repeat step #2 (keep repeating until the process is finished or an error has been received).
 
 @warning Identification is only available for the FaceAPI and SignatureAPI.
 */
@protocol IdentificationDelegate <NSObject>

/**
 Sent to the delegate when the identification operation has finished.
 
 @param result The result of the identification process.
 @param data The data returned by the identification operation.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_WRONG_PARAMETERS if there is a required input parameter that is missing or that it has a wrong format.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_IDENTIFY The identification process has not been started with the corresponding "startIdentification" operation.
 - MobbIDAPIErrorCode.ERROR_METHOD_NOT_SYNCHRONIZED The identification process cannot start in offline mode because the enrollment was done in online mode and the synchronization mode was disabled or because the synchronization process have failed
 
 @since 4.3
 */
- (void)identificationFinished:(MobbIDAPIIdentificationDelegateResult)result
                          data:(MobbIDAPIIdentificationResultData*)data
                         error:(NSError *)errorOccurred;

/**
 Sent to the delegate when the identification operation has finished.
 
 @param result The result of the identification process.
 @param users NSArray The array contains all candidates found with the given samples when the result is MobbIDAPIIdentificationDelegateResult_USER_IDENTIFIED, in any other case it will be nil. Each user of the array will be represented by an NSDictionary with two objects:
 
 - NSString with the userId for the key kMobbIDAPI_IdentificationUserKey
 - NSNumber with the score (float) for the key kMobbIDAPI_IdentificationScoreKey
 
 @param progressOK value from 0.0 to 100.0 that determines the progress to get a correct process.
 @param progressKO value from 0.0 to 100.0 that determines the progress to get a failed process.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_WRONG_PARAMETERS if there is a required input parameter that is missing or that it has a wrong format.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_IDENTIFY The identification process has not been started with the corresponding "startIdentification" operation.
 - MobbIDAPIErrorCode.ERROR_METHOD_NOT_SYNCHRONIZED The identification process cannot start in offline mode because the enrollment was done in online mode and the synchronization mode was disabled or because the synchronization process have failed
 
 @deprecated
 */
- (void)identificationFinished:(MobbIDAPIIdentificationDelegateResult)result
                         users:(NSArray *)users
                    progressOK:(float)progressOK
                    progressKO:(float)progressKO
                         error:(NSError *)errorOccurred;

@end
