//
//  MobbIDBaseOperationDelegate.h
//  MobbIDAPIDemo
//
//  Created by Abraham Holgado on 24/07/12.
//  Copyright 2012 Mobbeel Solutions. All rights reserved.
//

#import "MobbIDSDKTypes.h"

/** Key used to retrieve the user from the user's NSArray for the identification operation. */
extern const NSString *kMobbIDFramework_IdentificationUserKey;

/** Key used to retrieve the score of the user from the user's NSArray for the identification operation. */
extern const NSString *kMobbIDFramework_IdentificationScoreKey;

/**
 Object that holds the information returned by the enrollment process.
 
 @since 3.4
 */
@interface MobbIDSDKOperationEnrollmentResultData : NSObject

/** The user unique identification of the user that is being enrolled. */
@property (nonatomic, copy) NSString * userId;

/** The biometric method for the user's enrollment. */
@property (nonatomic, assign) MobbIDSDKBiometricMethod method;

/** The token returned by the operation. */
@property (nonatomic, copy) NSString * token;

/** The synchronization token returned by the operation. This token might be use later to retrieve the biometric template of the user and synchronize the online/offline modes */
@property (nonatomic, copy) NSString * synchronizationToken;

/** The biometric key of the user generated during the enrollment process. Its value dependens on the server configuration (Ask the support team for more information) */
@property (nonatomic, copy) NSString * key;

@end

/**
 Object that holds the information returned by the verification process.
 
 @since 3.4
 */
@interface MobbIDSDKOperationVerificationResultData : NSObject

/** The user unique identification of the user that is being verified. */
@property (nonatomic, copy) NSString * userId;

/** The biometric method for the user's verification. */
@property (nonatomic, assign) MobbIDSDKBiometricMethod method;

/** The token returned by the operation. */
@property (nonatomic, copy) NSString * token;

/** The synchronization token returned by the operation. This token might be use later to retrieve the biometric template of the user and synchronize the online/offline modes */
@property (nonatomic, copy) NSString * synchronizationToken;

/** The biometric key of the user re-generated during the verification process. Its value dependens on the server configuration (Ask the support team for more information) */
@property (nonatomic, copy) NSString * key;

@end


/**
 The delegate of a Biometric Recognition View (VoiceView, IrisView, SignatureView, FaceView and FaceVoiceView) must adopt this protocol to be informed of the result of the biometric operation that the biometric view is performing (enrollment, verification or identification). 
 */
@protocol BiometricMethodDelegate <NSObject>

@optional

/**
 Sent to the delegate when the enrollment operation has finished.
 
 @discussion In the next mayor release (4.0) this will be the only method available for the enrollment operation.
 
 @param resultCode MobbIDFrameworkOperationResult The result of the enrollment process performed by the biometric recognition view.
 @param data MobbIDSDKOperationEnrollmentResultData The data returned by the enrollment operation.
 @param errorOccurred NSError If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 
 @since 3.4
 */
- (void)enrollmentFinished:(MobbIDSDKOperationEnrollmentResult)resultCode
                      data:(MobbIDSDKOperationEnrollmentResultData *)data
                     error:(NSError *)errorOccurred;

/**
 Sent to the delegate when the enrollment operation has finished.
 
 @param userUUID NSString The unique identifier of the user that has been enrolled in the system.
 @param resultCode MobbIDFrameworkOperationResult The result of the enrollment process performed by the biometric recognition view.
 @param method MobbIDSDKBiometricMethod The biometric method in which the user has been enrolled in.
 @param errorOccurred NSError If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 
 @deprecated This method will be remove from the SDK in the next mayor release (4.0). Please use enrollmentFinishedForUser:result:method:data:error: instead.
 */
- (void)enrollmentFinishedForUser:(NSString *)userUUID
                           result:(MobbIDSDKOperationEnrollmentResult)resultCode
                           method:(MobbIDSDKBiometricMethod)method
                            error:(NSError *)errorOccurred;



/**
 Sent to the delegate when the verification operation has finished.
 
 @discussion In the next mayor release (4.0) this will be the only method available for the verification operation. 
 
 @param resultCode MobbIDSDKOperationVerificationResult The result of the verification process performed by the biometric recognition view.
 @param data MobbIDSDKOperationVerificationResultData The data returned by the verification operation.
 @param errorOccurred NSError If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 
 @since 3.4
 */
- (void)verificationFinished:(MobbIDSDKOperationVerificationResult)resultCode
                        data:(MobbIDSDKOperationVerificationResultData *)data
                       error:(NSError *)errorOccurred;

/**
 Sent to the delegate when the verification operation has finished.
 
 @param userUUID The unique identifier of the user that is trying to verify in the system.
 @param resultCode The result of the verification process performed by the biometric recognition view.
 @param method The biometric method in which the user has tried to verify in.
 @param token TODO. To be documented!
 @param errorOccurred NSError If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 
 @deprecated In a next mayor release, this enum would be removed.
 */
- (void)verificationFinishedForUser:(NSString *)userUUID
                             result:(MobbIDSDKOperationVerificationResult)resultCode
                             method:(MobbIDSDKBiometricMethod)method
                              token:(NSString *)token
                              error:(NSError *)errorOccurred;


/**
 Sent to the delegate when the identification operation has finished.
 
 @param users NSArray The array contains all candidates found with the given samples when the result is MobbIDAPIIdentificationDelegateResult_USER_IDENTIFIED, in any other case it will be nil. Each user of the array will be represented by an NSDictionary with two objects:
 
 - NSString with the userId for the key kMobbIDFramework_IdentificationUserKey
 - NSNumber with the score (float) for the key kMobbIDFramework_IdentificationScoreKey
 
 @param resultCode The result of the identification process performed by the biometric recognition view.
 @param method The biometric method in which the user has tried to identify in.
 @param errorOccurred NSError If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 
 @warning Identification mode is *only* avaiable for FaceView and SignatureView at the current version.
 */
- (void)identificationFinishedForUsers:(NSArray *)users
                                result:(MobbIDSDKOperationIdentificationResult)resultCode
                                method:(MobbIDSDKBiometricMethod)method
                                 error:(NSError *)errorOccurred;


@end
