//
//  SignatureAPI.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MobbIDBaseAPI.h"

#import "StartEnrollmentDelegate.h"
#import "EnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"
#import "StartIdentificationDelegate.h"
#import "IdentificationDelegate.h"

/**
 MobbID SignatureAPI provides methods to enroll and verify users using their handwritten signature as the biometric feature.
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 To be able to verify the identity of the users it is mandatory to enroll them previously on the system.
 
 Each biometric process (enrollment and verification) must be initiated with its corresponding "start" method before calling the "doEnrollmentOfUser..." or "doVerificationOfUser:..."
 
 @warning *Important:*Online vs Standlone (offline) mode.
 
 Currently the MobbIDAPI could work in both modes **but** some biometric methods are not supported in offline mode.
 
 SignatureAPI is *only* available for the online mode.
 */
@interface SignatureAPI : MobbIDBaseAPI

/**
 Initiates the enrollment process for the given user.
 
 @param userId The user to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startEnrollment:(NSString *)userId delegate:(id <StartEnrollmentDelegate>)delegate;

/** 
 Enrolls an existing user with one or more signatures previously acquired.
 
 @param userId The user to be enrolled.
 @param data NSArray containing one or more SignatureDataSet objects representing the user's signature.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doEnrollmentOfUser:(NSString*)userId 
                   andData:(NSArray*)data
                  delegate:(id <EnrollmentDelegate>)delegate;

/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString*)userId delegate:(id <StartVerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data NSArray containing one SignatureDataSet object representing the user's signature.
 @param type Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(NSArray*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data NSArray containing one SignatureDataSet object representing the user's signature.
 @param type Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType 
            forTransactionId:(NSString*)transactionId  
                     andData:(NSArray*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data NSArray containing one SignatureDataSet object representing the user's signature.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(NSArray*)data
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data NSArray containing one SignatureDataSet object representing the user's signature.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(NSArray*)data
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Initiates the identification process.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startIdentificationWithDelegate:(id <StartIdentificationDelegate>)delegate;

/**
 Try to identify an user using the signature.
 
 @param data The NSArray object representing the signatures previously acquired.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doIdentificationWithData:(NSArray*)data
                        delegate:(id <IdentificationDelegate>)delegate;

/**
 Try to identify an user using the signature.
 
 @param identificationType The action to perform when the identification is successful.The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of identification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the identification is successful. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The NSArray object representing the signatures previously acquired.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doIdentificationWithType:(MobbIDAPIVerificationType)identificationType
                forTransactionId:(NSString*)transactionId
                         andData:(NSArray*)data
                        delegate:(id <IdentificationDelegate>)delegate;

@end
