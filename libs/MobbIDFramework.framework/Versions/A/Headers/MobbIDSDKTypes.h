//
//  MobbIDSDKTypes.h
//  MobbIDSDKTypes
//
//  Created by Abraham Holgado García on 25/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Constant with the name of the domain for the errors returned by the SDK operations */
extern const NSString *kMobbIDSDKDomainError;

/** Constant with the name of the resource bundle packaged within the SDK */
extern const NSString *kMobbIDSDKBundleName;

/**
 Different types of speech modes that could be used to enroll (and verify) an user for the voice recognition method.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKVoiceRecognitionSpeechMode) {
    /** Random numbers will be presented to the user and he will have to repeat them. */
	MobbIDSDKVoiceRecognitionSpeechMode_RandomNumbers,
    /** The user will have to repeat the same passphrase during the enrollment and the verification processes */
	MobbIDSDKVoiceRecognitionSpeechMode_Passphrase,
    /** This option will let the user to say anything he wants until the system has captured enough voice to enroll or verify him */
	MobbIDSDKVoiceRecognitionSpeechMode_FreeSpeech
};

/**
 Different types of recognition modes available when working with the biometric recognition views avaiable in the MobbID Framework.
 
 @warning *Important:* Identification mode (MobbIDSDKRecognitionMode_Identification) is only available for the FaceView and SignatureView recognition view.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKRecognitionMode) {
    /** Recognition mode used for the enrollment operation */
	MobbIDSDKRecognitionMode_Enrollment,
    /** Recognition mode used for the verification operation */
	MobbIDSDKRecognitionMode_Verification,
    /** Recognition mode used for the identification operation */
	MobbIDSDKRecognitionMode_Identification,
    /** Recognition mode used to capture samples from the user */
	MobbIDSDKRecognitionMode_Capture
};

/**
 This enum is used to indicate the action that should be performed when a verification is successful.
 
 MobbIDSDKVerificationType_Simple is default value if no verification type is specified the verification.
 
 @deprecated This enum will not be used from 3.4 version and above. Please refer to the "Technical overview documentation" for more information about the new verification approach.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKVerificationType) {
	/** No further action if the user if verified */
    MobbIDSDKVerificationType_Simple,
    /** The user will be logged on the system */
	MobbIDSDKVerificationType_Login,
    /** To log the user for an specific transaction */
	MobbIDSDKVerificationType_Transaction,
    /** If the user is verified, the biometric key will be regenerated ant returned in the verification delegate */
	MobbIDSDKVerificationType_KeyRegeneration
};

/**
 This enum is used internally to specify the type of attempt the user is performing in every verification.
 
 @warning This is used for debug and test purposes and only when the `debug mode` is enabled. *It is not take into account in the actual biometric recognition.*
 */
typedef NS_ENUM(NSInteger, MobbIDSDKVerificationSampleAttempt) {
    /** The verification attempt is unknown. */
	MobbIDSDKVerificationSampleAttempt_Unknown,
    /** The verification attempt is perform by same user who has been previously been enrolled. */
	MobbIDSDKVerificationSampleAttempt_GenuineUser,
    /** The verification attempt is perform by an attacker. */
	MobbIDSDKVerificationSampleAttempt_CasualForgery,
    /** The verification attempt is perform by an skilled attacker */
	MobbIDSDKVerificationSampleAttempt_SkilledForgery
};

/**
 All possible operation results from MobbIDFramework enrollment operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKOperationEnrollmentResult) {
    /** The user has been properly enrolled. */
	MobbIDSDKOperationEnrollmentResult_USER_ENROLLED,
    /** There has been some error on the operation. */
	MobbIDSDKOperationEnrollmentResult_ERROR
};

/**
 All possible operation results from MobbIDFramework verification operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKOperationVerificationResult) {
    /** The user has been verified */
	MobbIDSDKOperationVerificationResult_USER_VERIFIED,
    /** The user has not been verified */
	MobbIDSDKOperationVerificationResult_USER_NOT_VERIFIED,
    /** There has been some error on the operation. */
	MobbIDSDKOperationVerificationResult_ERROR
};

/**
 All possible operation results from MobbIDFramework identification operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKOperationIdentificationResult) {
     /** The user has been identified  */
	MobbIDSDKOperationIdentificationResult_USER_IDENTIFIED,
     /** The user has not been identified */
	MobbIDSDKOperationIdentificationResult_USER_NOT_IDENTIFIED,
    /** There has been some error on the operation. */
	MobbIDSDKOperationIdentificationResult_ERROR
};

/**
 All possible operation results from MobbIDFramework capture operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKOperationCaptureResult) {
    /** The sample has been properly captured. */
	MobbIDSDKOperationCaptureResult_DATA_CAPTURED,
    /** This could happen when the app does not have the right permission to capture the sample (microphone...) */
	MobbIDSDKOperationCaptureResult_ERROR
};


/**
 All possible errors that can happen in a MobbID Framework operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKErrorCode) {
    /** Un unexpected or uncontrolled error as happen while performing an SDK operation. */
	MobbIDSDKErrorCode_ERROR_UNEXPECTED = 0,
    /** It happens when there was an error with the connection (connection refused, time out, etc) */
	MobbIDSDKErrorCode_ERROR_CONNECTION = 1,
    /** If the license provided in the SDK's setting is invalid or has expired */
	MobbIDSDKErrorCode_ERROR_LICENSE = 2,
    /** If the userId parameter is not present in the request */
	MobbIDSDKErrorCode_ERROR_USER_ID_REQUIRED = 3,
    /** If the user specified in the operation does not exist on the system */
	MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST = 4,
    /** If the user trying to enroll in the biometric method is already enrolled in it (and the biometric method does not have the adaptative enrollment enabled)*/
	MobbIDSDKErrorCode_ERROR_USER_ALREADY_ENROLLED_IN_METHOD = 5,
    /** If the user is trying to be verified in a biometric method in which he/she has not been enrolled yet */
	MobbIDSDKErrorCode_ERROR_USER_NOT_ENROLLED_ON_THIS_METHOD = 6,
    /** If the operation is not available (e.g: iris recognition in offline mode) */
	MobbIDSDKErrorCode_ERROR_NOT_AVAILABLE_IN_CURRENT_MODE = 7,
    /** If there has been some error on the biometric samples used in the biometric process */
	MobbIDSDKErrorCode_ERROR_ON_SAMPLES = 8,
    /** If there has been some error on the speech recognition engine */
	MobbIDSDKErrorCode_ERROR_SPEECH_RECOGNITION_PROBLEM = 9,
    /** If the verification process cannot be performed in offline mode because the user's enrollment was done in online mode and the synchronization mode was disabled or because the synchronization process itself have failed */
	MobbIDSDKErrorCode_ERROR_METHOD_NOT_SYNCHRONIZED = 10,
    /** If there has been some error when customizing a Biometric View such as the value is incompatible with the property or the property specified does not exist */
	MobbIDSDKErrorCode_ERROR_CUSTOMIZATION_PROPERTY = 11,
    /** If the user cannot perform an operation. It could happen, for instance, when a biometric process cannot be done because the device does not let the SDK to use the camera or microphone...  */
	MobbIDSDKErrorCode_ERROR_PERMISSION_DENIED = 12
};

/**
 Different biometric methods available in the MobbID Framework.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKBiometricMethod) {
    /// Iris method
	MobbIDSDKBiometricMethod_METHOD_IRIS,
    /// Face method
	MobbIDSDKBiometricMethod_METHOD_FACE,
    /// Signature method
	MobbIDSDKBiometricMethod_METHOD_SIGNATURE,
    /// Voice method
	MobbIDSDKBiometricMethod_METHOD_VOICE,
    /// Combination of Face and Voice methods (only for verification)
	MobbIDSDKBiometricMethod_METHOD_VOICE_FACE
};
