//
//  IrisView.h
//  MobbIDFramework
//
//  Created by Raul Jareño diaz on 12/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "MobbIDSDKProgressUpdateDelegate.h"

/**
 The delegate of a Iris capture sample View (IrisView) must adopt this protocol to receive the samples of the biometric view. 
 */
@protocol IrisCaptureSampleDelegate <NSObject>

@required

/** 
 Callback method invoked when the irises samples has been acquired.
 
 @param eyes NSArray NSArray containing one image (UIImage) of every iris of the user isolated that has been isolated from the capture taken.
 */
- (void)irisSampleCaptured:(NSArray*)eyes;

@end

/**
 IrisView is the Biometric Recognition View that should be used to enroll (register) or verify a user using his/her irises as the biometric method.
 
 Before performing any biometric operation (enrollment or verification) please ensure that:
 
 - The user exists (@see MobbIDManagementAPI for more information)
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the IrisView to enroll an existing user you must follow these steps:
 
 1) Create and init IrisView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `IrisView *irisView = [[IrisView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[irisView setDelegate:self];'
 
 3) Add the IrisView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:irisView];`
 
 4) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[irisView startRecognitionIn:MobbIDSDKRecognitionMode_Enrollment forUser:USERID];`
 
 5) Stop the biometric recognition process when it is finished (method enrollmentFinishedForUser:result:method: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[irisView stopRecognition];`
 
 To use the IrisView to verify a previously enrolled user you follow the previous steps and specify the verification type:
 
 `// irisView is init and added as a subview to the ViewController's view.`
 
 `[irisView setVerificationType:MobbIDSDKVerificationType_Simple];`
 
 `[irisView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 To use the IrisView to capture the samples you have to set up its captureDelegate, conforming the IrisCaptureSampleDelegate. The captureDelegate does not have to be the ViewController (it could be any class conforming the IrisCaptureSampleDelegate).
 
 `[irisView setCaptureDelegate:self];'
 
 If you dont want to perform the recognition you have to use the MobbIDSDKRecognitionMode_Capture in the method startRecognitionIn:forUser:
 
 `[irisView startRecognitionIn:MobbIDSDKRecognitionMode_Capture forUser:USERID];`
 
 */
@interface IrisView : MobbIDSDKBaseView

/**
 Set this delegate if you want to get the sample information of each iris captured.
 */
@property (nonatomic, weak) id<IrisCaptureSampleDelegate> captureDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the recognition progress.
 */
@property (nonatomic, weak) id<MobbIDSDKProgressUpdateDelegate> progressUpdateDelegate;

@property (nonatomic) BOOL localFeaturesAcquisition;

@end