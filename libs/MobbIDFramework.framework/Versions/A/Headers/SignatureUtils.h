//
//  SignatureUtils.h
//  MobbIDAPI
//
//  Created by Abraham Holgado Garcia on 12/08/13.
//
//

#import <Foundation/Foundation.h>

@class SignatureBiometricTemplate;
@class BIOMETRIC_TEMPLATE;
@class SignatureDataSet;

/**
 Utility class to perform common operations with the signature.
 */
@interface SignatureUtils : NSObject

/**
 Transform the signature from a SignatureDataSet to a JSON NSString representation.
 
 @param signatureDataSet Signature object represented in the SignatureDataSet format.
 
 @return NSString with the JSON representation of the signature received.
 */
+ (NSString *) jsonSignatureFrom:(SignatureDataSet *)signatureDataSet;

/**
 Transform the signature from a NSArray of SignatureDataSet to a JSON NSString representation.
 
 @param signatureDataSetArray NSArray of SignatureDataSet object. Each SignatureDataSet represents an user's signature.
 
 @return NSString with the JSON representation of the signatures received.
 */
+ (NSString *) jsonSignaturesFrom:(NSArray *)signatureDataSetArray;


/**
 Transform a Biometric template stored in the database to a SignatureBiometricTemplate.
 
 @param bt BIOMETRIC_TEMPLATE
 
 @return SignatureBiometricTemplate
 */
+ (SignatureBiometricTemplate*) getSignatureBiometricTemplate:(BIOMETRIC_TEMPLATE*) bt;

@end
