//
//  FaceView.h
//  MobbIDFramework
//
//  Created by Raul Jareño diaz on 16/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "MobbIDSDKProgressUpdateDelegate.h"

/**
 The delegate of a Face capture sample View (FaceView) must adopt this protocol to receive the samples of the biometric view.
 */
@protocol FaceCaptureSampleDelegate <NSObject>

@required

/**
 This method gets called each time a face is capture and use for the recognition process.
 
 @param face UIImage Image of the user's face.
 */
- (void)faceSampleCaptured:(UIImage *)face;

@end

/**
 This delegate is useful when you need to be informed of the face detection process during recognition.
 */
@protocol FaceDetectionDelegate <NSObject>

@optional

/**
 This method gets called each time a face is detected in the image.
 
 @param faceRect The rect coordinates of the face, relatives to the FaceView frame.
 @param distanceOk Indicates if the distance between the face and the device is enough to perform the recognition.
 */
- (void)onFaceDetected:(CGRect)faceRect distanceOk:(BOOL)distanceOk;

/**
 This methods gets called each time a face is not detected in the image.
 */
- (void)onNoFaceDetected;

@end

/**
 FaceView is the Biometric Recognition View that should be used to enroll (register) or verify a user using his/her face as the biometric method.
 
 Before performing any biometric operation (enrollment or verification) please ensure that:
 
 - The user exists (@see MobbIDManagementAPI for more information)
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the FaceView to enroll an existing user you must follow these steps:
 
 1) Create and init FaceView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `FaceView *faceView = [[FaceView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[faceView setDelegate:self];`
 
 3) Add the FaceView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:faceView];`
 
 4) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[faceView startRecognitionIn:MobbIDSDKRecognitionMode_Enrollment forUser:USERID];`
 
 5) Stop the biometric recognition process when it is finished (method enrollmentFinishedForUser:result:method: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[faceView stopRecognition];`
 
 To use the FaceView to verify a previously enrolled user you follow the previous steps and specify the verification type:
 
 ` // faceView is init and added as a subview to the ViewController's view.`
 
 `[faceView setVerificationType:MobbIDSDKVerificationType_Simple];`
 
 `[faceView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 
 To use the FaceView to identify a previously enrolled user you follow the previous steps and specify the identification type:
 
 `// faceView is init and added as a subview to the ViewController's view.`
 
 `[faceView startRecognitionIn:MobbIDSDKRecognitionMode_Identification forUser:nil];`
 
 To use the FaceView to capture the samples you have to set up its captureDelegate, conforming the FaceCaptureSampleDelegate. The captureDelegate does not have to be the ViewController (it could be any class conforming the FaceCaptureSampleDelegate).
 
 `[faceView setCaptureDelegate:self];'
 
 If you dont want to perform the recognition you have to use the MobbIDSDKRecognitionMode_Capture in the method startRecognitionIn:forUser:
 
 `[faceView startRecognitionIn:MobbIDSDKRecognitionMode_Capture forUser:USERID];`
 
 */
@interface FaceView : MobbIDSDKBaseView

/**
 Set this delegate if you want to get the sample information of each face captured.
 */
@property (nonatomic, weak) id <FaceCaptureSampleDelegate> captureDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the recognition progress.
 */
@property (nonatomic, weak) id <MobbIDSDKProgressUpdateDelegate> progressUpdateDelegate;

/**
 Set this delegate if you want to be informed of the face detection process.
 */
@property (nonatomic, weak) id <FaceDetectionDelegate> faceDetectionDelegate;

@end
