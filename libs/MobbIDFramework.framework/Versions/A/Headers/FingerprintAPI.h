//
//  FingerprintAPI.h
//  MobbIDAPI
//
//  Created by Raul Jareno on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MobbIDBaseAPI.h"

#import "StartEnrollmentDelegate.h"
#import "EnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"

/**
 MobbID FingerprintAPI provides methods to enroll and verify users using their fingerprint as the biometric feature.
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 To be able to verify the identity of the users it is mandatory to enroll them previously on the system.
 
 Each biometric process (enrollment and verification) must be initiated with its corresponding "start" method before calling the "doEnrollmentOfUser..." or "doVerificationOfUser:..."
 
 @warning *Important:*Online vs Standlone (offline) mode.
 
 Currently the MobbIDAPI could work in both modes **but** some biometric methods are not supported in offline mode.
 
 FingerprintAPI is available for both, **online** and **offline**, modes.
 */
@interface FingerprintAPI : MobbIDBaseAPI

/**
 Initiates the enrollment process for the given user.
 
 @param userId The user to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startEnrollment:(NSString *)userId delegate:(id <StartEnrollmentDelegate>)delegate;

/** 
 Enrolls an existing user with one or more signatures previously acquired.
 
 @param userId The user to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doEnrollmentOfUser:(NSString*)userId
                  delegate:(id <EnrollmentDelegate>)delegate;

/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString*)userId delegate:(id <StartVerificationDelegate>)delegate;

/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified.
 @param type Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString*)userId delegate:(id <StartVerificationDelegate>)delegate attemptType:(MobbIDAPIVerificationSampleAttempt)type;

/**
 Try to verify the identity of an existing user using his fingerprint.
 
 @param userId The user to be verified.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 4.0
 */
- (void)doVerificationOfUser:(NSString*)userId
                    delegate:(id <VerificationDelegate>)delegate;


@end
