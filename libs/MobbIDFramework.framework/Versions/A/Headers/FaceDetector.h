//
//  FaceDetector.h
//  MobbIDAPI
//
//  Created by Abraham Holgado García on 11/01/12.
//  Copyright (c) 2012 Mobbeel Solutions. All rights reserved.
//

@class UIImageUtils;

/**
 Class used to detect faces in a given image or in a given buffer.
 */
@interface FaceDetector : NSObject {
    
    @private
    UIImageUtils * imageUtils;    
}

/** 
 Detects faces in the given image.
 It will return an NSArray of CGRect wrapped as NSValue object with the coordinates of all posible faces detected.
 If no face is detected an empty array is returned.
 The NSArray is ordered from "best candidate face" (position 0) to "worst candidate face".
 
 @param image The UIImage to detect faces in.
 */
- (NSArray *) detectFaceInImage:(UIImage *)image;

/** 
 Detects faces in the given image. This method is intented to be use with the data collected from AVCaptureVideoDataOutputSampleBufferDelegate methods AVCaptureSession).
 It will return an NSArray of CGRect wrapped as NSValue object with the coordinates of all posible faces detected.
 If no face is detected an empty array is returned.
 The NSArray is ordered from "best candidate face" (position 0) to "worst candidate face".
 
 @param pixelBuffer The base address for the pixel buffer (CVPixelBufferRef) that can be retrieved from the CMSampleBufferRef.
 @param format The OSType that can be retrieved from the pixel buffer (CVPixelBufferGetPixelFormatType(pixelBuffer)).
 @param rect CGRect to be documented!
 @param videOrientation The AVCaptureVideoOrientation from the AVCaptureVideoDataOutput of the AVCaptureSession
 */
- (NSArray *) detectFaceInPixelBuffer:(void *)pixelBuffer format:(OSType)format videoRect:(CGRect)rect videoOrientation:(NSInteger)videOrientation;

@end
