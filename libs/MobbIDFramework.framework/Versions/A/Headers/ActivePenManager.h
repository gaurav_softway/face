//
//  ActivePenManager.h
//  MobbSignSDK
//
//  Created by Rodrigo Sánchez González on 08/05/13.
//  Copyright (c) 2013 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef ACTIVE_PEN_MANAGER
#define ACTIVE_PEN_MANAGER

/** 
 Active pen currently supported in Mobbeel's products.
 */
typedef NS_ENUM(NSInteger, ActivePenType) {
    /** [Pogo homepage](https://www.tenonedesign.com/connect.php) */
    ActivePenTypePogo,
    /** [JotTouch homepage](http://www.adonit.net/jot/touch/) */
    ActivePenTypeJotTouch
};

/**
 This protocol is used internally to as a bridge between the `ActivePenManager` and the Active Pen (Pogo or JotTouch) you used in your project.
 */
@protocol PrivateActivePenManagerDelegate <NSObject>

@optional

/**
 This method is invoked when a UITouch is captured by the active pen.
 
 @param touch UITouch with the information received from the active pen
 */
- (void) touchFromPen:(UITouch *)touch;

/**
 This method is invoked when the connection with the active pen has been successfull
 
 @param penType ActivePenType
 */
- (void) activePenDidConnect:(ActivePenType)penType;

/**
 This method is invoked when the active pen has been disconnected
 
 @param penType ActivePenType
 */
- (void) activePenDidDisconnect:(ActivePenType)penType;

/**
 This method is invoked when the active pen specified in the initialization of the `ActivePenManager` object is not supported by the device.
 
 @param penType ActivePenType
 */
- (void) activePenNotSupported:(ActivePenType)penType;

@end

/**
 This class is intented to let the invoker to use one the active pens currently supported by Mobbeel.
 */
@interface ActivePenManager : NSObject

/** Use this property to change the `PrivateActivePenManagerDelegate` and get informed of the state of the active pen you're using */
@property (nonatomic, weak) id<PrivateActivePenManagerDelegate> delegate;

/** Property with ActivePenType specified in the class initialization */
@property (nonatomic, readonly) ActivePenType penType;

/** The UIView in which you want to use the active pen*/
@property (nonatomic, weak) UIView *view;

/**
 This method **must** be used to init the object
 
 @param view UIView in which you want to use the active pen
 @param del PrivateActivePenManagerDelegate is the delegate what will be informed of the active pen status (and will received UITouchs from it)
 @param type ActivePenType that you want to use
 */
- (id) initActivePenManager:(UIView*)view delegate:(id<PrivateActivePenManagerDelegate>)del activePenType:(ActivePenType)type;

/**
 Method to check if a UITouch comes from an active pen or not.
 
 @param touch UITouch to be checked
 
 @return BOOL YES if the touch parameters comes from an active pen. NO Otherwise
 */
- (BOOL) touchIsPen:(UITouch *)touch;

/**
 Use this method to obtain the real pressure from a UITouch you've obtain with the `PrivateActivePenManagerDelegate` delegate
 
 @param touch UITouch to obtain the pressure from
 
 @return float returns the real pressure (if available) from the given UITouch
 */
- (float) realPressureForTouch:(UITouch *)touch;

/**
 Use this method to obtain the an optimal pressure value from a UITouch you've obtain with the `PrivateActivePenManagerDelegate` delegate. It is intented to be used for drawning and the value could be normalized.

 @param touch UITouch to obtain the pressure from
 
 @return float returns the drawning pressure
 */
- (float) pressureForDrawing:(UITouch *)touch;

/**
 Method to check if an active pen is connected to the device.
 
 @return BOOL Yes if the active pen is currently connected. NO otherwise
 */
- (BOOL) isPenConnected;

/**
 Invoke this method to disconnect the active ven.
 */
- (void) disconnectPen;

/**
 Use this method to obtain the a normalized drawing pressure value from a regular pressure value. It will take into account the Active pen technology under the hoods to normalize the value.
 
 @param pressure float with the real pressure of the touch
 
 @return float returns a normalized drawning pressure.
 */
- (float) getDrawingPressureFromRealPressure:(float)pressure;

@end

#endif
