//
//  MobbIDBaseAPI.h
//  MobbIDBaseAPI
//
//  Created by Abraham Holgado García on 29/03/12.
//  Copyright (c) 2012 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StartEnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "DownloadTemplateDelegate.h"

@class MobbIDASIHTTPRequest;

/**
 Gender types supported by the MobbIDAPI
 */
typedef NS_ENUM(NSInteger, MobbIDAPIGender){
    MobbIDAPIGender_Male = 0,
    MobbIDAPIGender_Female = 1
};

/**
 Current languages supported by the MobbIDAPI
 */
typedef NS_ENUM(NSInteger, MobbIDAPISupportedLanguage){
    MobbIDAPISupportedLanguage_English = 0,
    MobbIDAPISupportedLanguage_Spanish = 1,
    MobbIDAPISupportedLanguage_German = 2,
    MobbIDAPISupportedLanguage_French = 3,
    MobbIDAPISupportedLanguage_Russian = 4
};
                
/**
 The different types of attempts that could be used in a verification
 ** This is only used for debug purposes **
 */
typedef NS_ENUM(NSInteger, MobbIDAPIVerificationSampleAttempt) {
    MobbIDAPIVerificationSampleAttempt_Unknown = -1,
    MobbIDAPIVerificationSampleAttempt_GenuineUser = 0,
    MobbIDAPIVerificationSampleAttempt_CasualForgery = 1,
    MobbIDAPIVerificationSampleAttempt_SkilledForgery = 2
};


/**
 This enum is used to indicate the action that should be performed when a verification is successful.

 @deprecated In a next mayor release, this enum would be removed.
 */
typedef NS_ENUM(NSInteger, MobbIDAPIVerificationType) {
    /** No further action. */
    MobbIDAPIVerificationType_Simple = 0,
    /** Log the user into the system. */
    MobbIDAPIVerificationType_Login = 1,
    /** Log the user into the system for a specific operation. */
    MobbIDAPIVerificationType_Transaction = 2,
    /** Regenerates the user's key. */
    MobbIDAPIVerificationType_KeyRegeneration = 3,
};

/**
 Error codes for NSError used by the MobbIDAPI
 */
typedef NS_ENUM(NSInteger, MobbIDAPIErrorCode) {
    // commons
    ERROR_UNEXPECTED = 0,
    ERROR_CONNECTION = 1,
    ERROR_INTERNAL_SERVER_ERROR = 2,
    ERROR_WRONG_PARAMETERS = 3,
    ERROR_NOT_AVAILABLE_IN_CURRENT_MODE = 4,
    ERROR_USER_FIELD_REQUIRED = 5,
    ERROR_USER_DOES_NOT_EXIST = 6,
    // checkCreateUser
    ERROR_USER_IS_NOT_ACTIVATED = 7,
    // checkLicense    
    ERROR_LICENSE_FIELD_REQUIRED = 8,
    ERROR_LICENSE_FILE_NOT_FOUND = 9,
    ERROR_LICENSE_WRONG_LICENSE_ID = 10,
    ERROR_LIMIT_USERS_EXCEEDED = 11,
    ERROR_LICENSE_EXPIRED = 12,
    ERROR_LICENSE_WRONG_FORMAT = 13,
    ERROR_LICENSE_PERIOD_IS_NOT_ACTIVE_YET = 14,
    // checkOperationAuthrization
    ERROR_OPERATION_TYPE_DOES_NOT_EXIST = 15,
    ERROR_OPERATION_TYPE_FIELD_NOT_VALID = 16,
    // confirmUser
    ERROR_TOKEN_IS_NOT_VALID = 17,
    ERROR_TOKEN_HAS_EXPIRED = 18,
    ERROR_USER_AND_TOKEN_COMBINATION_NOT_VALID = 19,
    // startEnrollment
    ERROR_METHOD_NOT_AVAILABLE_FOR_THIS_LICENSE = 20,
    // enrollment
    ERROR_ON_SAMPLES = 21,
    ERROR_USER_NOT_ALLOWED_TO_ENROLL = 22,
    ERROR_USER_ALREADY_ENROLLED_IN_METHOD = 23,
    // startVerification
    // verification
    ERROR_USER_NOT_ALLOWED_TO_VERIFY = 24,
    ERROR_USER_NOT_ENROLLED_ON_THIS_METHOD = 25,
    // identification
    ERROR_USER_NOT_ALLOWED_TO_IDENTIFY = 26,
    //synchronization
    ERROR_METHOD_NOT_SYNCHRONIZED = 27
};

/// Values for NSError returned by the API in method errorOccurred:
extern const NSString * kMobbIDAPIDomainError;
extern const NSString * kMobbIDAPIExtraInfoKeyError;

/// Available biometric methods in the SDK.
extern const NSString * kMobbIDAPI_BiometricMethod_Signature;
extern const NSString * kMobbIDAPI_BiometricMethod_Iris;
extern const NSString * kMobbIDAPI_BiometricMethod_Face;
extern const NSString * kMobbIDAPI_BiometricMethod_Voice;
extern const NSString * kMobbIDAPI_BiometricMethod_VoiceFace;

/// Status code
extern const NSInteger kMobbIDAPI_STATUS_REQUEST_OK;
extern const NSInteger kMobbIDAPI_STATUS_BAD_REQUEST;
extern const NSInteger kMobbIDAPI_STATUS_FORBIDDEN_REQUEST;
extern const NSInteger kMobbIDAPI_STATUS_NOT_FOUND_REQUEST;
extern const NSInteger kMobbIDAPI_STATUS_INTERNAL_SERVER_ERROR;
extern const NSInteger kMobbIDAPI_STATUS_CONNECTION_ERROR;
extern const NSInteger kMobbIDAPI_STATUS_SERVER_NOT_REACHED_ERROR;


/**
 Base class for all the Recognition Biometric Methods in the MobbID API.
 
 So far, it only provides subclasses with some utility methods for handling common errors.
 */
@interface MobbIDBaseAPI : NSObject 

/** Property used internally to hold a reference of the delegate of some of the operations performed by the API  */
@property (nonatomic, weak) id delegate;

/**
 Initiates the enrollment process for the given user.
 
 @param userId The user to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startEnrollment:(NSString *)userId delegate:(id <StartEnrollmentDelegate>)delegate;


/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString *)userId delegate:(id <StartVerificationDelegate>)delegate;

/**
 Initiates the download process of the biometric template for the given user.
 
 @param userId The user of the biometric template.
 @param token The token received after a successfully online enrollment or verification.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)downloadTemplate:(NSString *)userId
               withToken:(NSString *)token
                delegate:(id <DownloadTemplateDelegate>)delegate;

/**
 Saves the biometricTemplate for the given user in the given biometricMethod. 
 
 @param biometricTemplate the biometric template to save.
 @param userId The user of the biometric template
 @param biometricMethod The biometric method. Use one of these values:
 
 - kMobbIDAPI_BiometricMethod_Signature
 - kMobbIDAPI_BiometricMethod_Voice
 - kMobbIDAPI_BiometricMethod_Face
 
 @param timestamp The timestamp returned in the method [DownloadTemplateDelegate downloadTemplateFinished:userId:bioTemplate:biometricMethod:timestamp:enrollType:error]
 @param enrollType Enrollment type return in the method [DownloadTemplateDelegate downloadTemplateFinished:userId:bioTemplate:biometricMethod:timestamp:enrollType:error]
 
 @warning If the user already has an offline enrollment for the biometricMethod it will be overwritten with the new biometricTemplate.
 */
- (BOOL) saveTemplate:(NSData *)biometricTemplate
                 user:(NSString *)userId
      biometricMethod:(NSString *)biometricMethod
            timestamp:(NSString *)timestamp
           enrollType:(NSString *)enrollType;
/**
 Checks if the user has an offline biometric template available for the given method.
 
 @param userId The user of the biometric template.
 @param biometricMethod The biometric method to check. Use one of these values:
 
 - kMobbIDAPI_BiometricMethod_Signature
 - kMobbIDAPI_BiometricMethod_Voice
 - kMobbIDAPI_BiometricMethod_Face

 */
- (BOOL) templateExistsForUser:(NSString *)userId forMethod:(NSString *)biometricMethod;


@end
