//
//  IrisAPI.h
//  MobbIDAPI 
//
//  Created by Abraham Holgado on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MobbIDBaseAPI.h"

#import "StartEnrollmentDelegate.h"
#import "EnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"

/**
 This enum is used to indicate how the led flash (if available) should work on the recognition process (enrollment or verification).
*/
typedef NS_ENUM(NSInteger, FlashMode) {
    /** The led flash will be used in torch mode (always on) */
	FLASH_TORCH,
    /** The led flash will be used in automatic mode */
	FLASH_AUTOMATIC,
    /** The led won't be used */
	NO_FLASH
};

/** 
 Information used when the eyes are detected in the recognition process 
 */
typedef NS_ENUM(NSInteger, EyeInformation) {
    /** The left eye has been detected */
    EYE_LEFT,
    /** The right eye has been detected */
    EYE_RIGHT,
    /** There is no information about which eye has been detected */
    EYE_UNKNOWN
};

/**
 MobbID IrisAPI provides methods to enroll and verify users using their irises as the biometric feature.
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 To be able to verify the identity of the users it is mandatory to enroll them previously on the system.
 
 Each biometric process (enrollment and verification) must be initiated with its corresponding "start" method before calling the "doEnrollmentOfUser..." or "doVerificationOfUser:..."
 
 @warning Online vs Offline mode.
 
 Currently the MobbIDAPI could work in both modes **but** some biometric methods are not supported in offline mode.
 
 #IrisAPI is only available for online mode.#
 */
@interface IrisAPI : MobbIDBaseAPI

/**
 Initiates the enrollment process for the given user.
 
 @param userId The user to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startEnrollment:(NSString *)userId delegate:(id <StartEnrollmentDelegate>)delegate;

/**
 Enrolls an existing user with an image previously acquired of the user's irises.
 
 @param userId The user to be enrolled.
 @param data The UIImage object containing the irises of the user.
 @param processed BOOL to be documented!
 @param delegate The delegate to be informed with the result of the operation.
 
 // TODO Explain the enrollment policy for one and two irises at the same time.
 
 */
- (void)doEnrollmentOfUser:(NSString*)userId 
                   andData:(NSArray*)data
             irisProcessed:(BOOL)processed
                  delegate:(id <EnrollmentDelegate>)delegate;

/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified. 
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString*)userId delegate:(id <StartVerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his irises.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage object that contains the iris(es) of the user
 @param processed BOOL to be documented!
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use MobbIDAPIVerificationSampleAttempt_Unknown
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId 
                    withType:(MobbIDAPIVerificationType)verificationType 
            forTransactionId:(NSString*)transactionId 
                     andData:(NSArray *)data
               irisProcessed:(BOOL)processed
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his irises.
 
 @param userId The user to be verified.
 @param data The UIImage object that contains the iris(es) of the user
 @param processed BOOL to be documented!
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use MobbIDAPIVerificationSampleAttempt_Unknown
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doVerificationOfUser:(NSString*)userId
                   withData:(NSArray *)data
               irisProcessed:(BOOL)processed
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his irises.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage object that contains the iris(es) of the user
 @param processed BOOL to be documented!
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(NSArray *)data
               irisProcessed:(BOOL)processed
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his irises.
 
 @param userId The user to be verified.
 @param data The UIImage object that contains the iris(es) of the user
 @param processed BOOL to be documented!
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(NSArray *)data
               irisProcessed:(BOOL)processed
                    delegate:(id <VerificationDelegate>)delegate;

@end
