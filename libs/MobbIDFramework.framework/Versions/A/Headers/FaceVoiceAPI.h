//
//  FaceVoiceAAPI.h
//  MobbIDAPI
//
//  Created by darthhyoga on 05/06/13.
//
//

#import <Foundation/Foundation.h>

#import "MobbIDBaseAPI.h"

#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"

/**
 MobbID `FaceVoiceAPI` *only* provides methods to **verify users** identity based on their voice and faces as the biometric features
 
 To be able to verify the identity of the users it is mandatory to enroll them previously on the system using the FaceAPI and VoiceAPI independently
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 Each verification process must be initiated with the `startVerification:delegate:` method before calling the `doVoiceVerificationOfUser:...` and `doVoiceVerificationOfUser:...`
 
 @warning *Important:* Online vs Offline mode.
 
 Currently MobbIDAPI could work in both modes **but** some biometric methods are not supported in standalone mode.
 
 Face & Voice simultaneously (FaceVoiceAPI) could work in both modes but there are some differences that must be taken into accout:
 
 - Online mode:
   - Face&Voice could work in several combination modes (It depends on the server configuration):
     - AND. The user must be verified in both methods to be verified.
     - OR. The user would be verified if face or voice are verified.
     - AVERAGE. The user would be verified depending on his scores in both methods. An algorithm would decide whether or not the user should be verified.
 - Offline mode:
   - Face&Voice would only work for now in "AND" mode.
 
 */
@interface FaceVoiceAPI : MobbIDBaseAPI

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The NSData object representing a voice previously acquired.
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. MobbIDAPIVerificationSampleAttempt_Unknown
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 
  @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVoiceVerificationOfUser:(NSString*)userId
                         withType:(MobbIDAPIVerificationType)verificationType
                 forTransactionId:(NSString*)transactionId
                          andData:(NSData*)data
                      attemptType:(MobbIDAPIVerificationSampleAttempt)type
                       flacFormat:(BOOL)flac
                         delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data The NSData object representing a voice previously acquired.
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. MobbIDAPIVerificationSampleAttempt_Unknown
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 
 */
- (void)doVoiceVerificationOfUser:(NSString*)userId
                         withData:(NSData*)data
                      attemptType:(MobbIDAPIVerificationSampleAttempt)type
                       flacFormat:(BOOL)flac
                         delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The NSData object representing a voice previously acquired.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVoiceVerificationOfUser:(NSString*)userId
                         withType:(MobbIDAPIVerificationType)verificationType
                 forTransactionId:(NSString*)transactionId
                          andData:(NSData*)data
                       flacFormat:(BOOL)flac
                         delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data The NSData object representing a voice previously acquired.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doVoiceVerificationOfUser:(NSString*)userId
                         withData:(NSData*)data
                       flacFormat:(BOOL)flac
                         delegate:(id <VerificationDelegate>)delegate;


/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage objects containing the face
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use MobbIDAPIVerificationSampleAttempt_Unknown
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doFaceVerificationOfUser:(NSString*)userId
                        withType:(MobbIDAPIVerificationType)verificationType
                forTransactionId:(NSString*)transactionId
                         andData:(UIImage*)data
                     attemptType:(MobbIDAPIVerificationSampleAttempt)type
                        delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param data The UIImage objects containing the face
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value, it is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use MobbIDAPIVerificationSampleAttempt_Unknown
 @param delegate The delegate to be informed with the result of the operation.
 
 */
- (void)doFaceVerificationOfUser:(NSString*)userId
                        withData:(UIImage*)data
                     attemptType:(MobbIDAPIVerificationSampleAttempt)type
                        delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage objects containing the face
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doFaceVerificationOfUser:(NSString*)userId
                        withType:(MobbIDAPIVerificationType)verificationType
                forTransactionId:(NSString*)transactionId
                         andData:(UIImage*)data
                        delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param data The UIImage objects containing the face
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doFaceVerificationOfUser:(NSString*)userId
                        withData:(UIImage*)data
                        delegate:(id <VerificationDelegate>)delegate;

- (NSArray *) generateTextNumbersForLanguage:(MobbIDAPISupportedLanguage)language;

- (NSArray *) generateTextForLanguage:(MobbIDAPISupportedLanguage)language;

@end
