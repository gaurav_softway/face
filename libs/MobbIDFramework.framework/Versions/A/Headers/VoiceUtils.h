//
//  VoiceDetector.h
//  MobbIDAPI
//
//  Created by Rodrigo Sánchez González on 16/04/13.
//
//

#import <Foundation/Foundation.h>

/// Sample rate used to capture the user's voice.
extern const long kMobbIDAPI_SAMPLE_RATE;

/// Number of channels used to capture the user's voice
extern const long kMobbIDAPI_CHANNELS;

/// Number of bits per channel used to capture the user's voice
extern const long kMobbIDAPI_BITS_PER_SAMPLE;

/// Header length of the wav file
extern const long kMobbIDAPI_WAV_HEADER_LENGHT;

/**
 Utility class for the VoiceAPI
 */
@interface VoiceUtils : NSObject

/**
 Method to copy the voice resources needed to perform the biometric recognition to the expected location
 */
+ (void) copyVoiceResources;

/**
 Method to clear the cache that is used in some operations of the biometric recognition
 */
+ (void) clearCache;

/**
 Checks if in the file received the speech of the user has finished.
 
 @param audioFile NSURL with the voice sample of the user
 
 @return BOOL YES if the end of speech has been detected.
 */
+ (BOOL)isEndOfSpeech:(NSURL*) audioFile;

/**
 Calculates the segments of speech available on the input parameter.
 
 @param audioFile NSURL with the voice sample of the user
 
 @return NSDictionary with the segments of speech available.
 */
+ (NSDictionary *)calculateBeginAndEndOfSpeech:(NSURL*) audioFile;

@end
