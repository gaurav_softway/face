//
//  SignatureAcquisitionView.h
//  MobbSign
//
//  Created by Rodrigo Sánchez González on 21/12/12.
//  Copyright (c) 2012 Mobbeel Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivePenManager.h"
#import "SignatureDataSet.h"

/**
 Protocol to receive information about the use of the active pen specified in the `activePenType` property of the `SignatureAcquisitionView`
 */
@protocol ActivePenManagerDelegate <NSObject>

@optional

/**
 * Method to inform that the manager is searching for a pen to connect
 */
- (void) onSearchingForPen;

/**
 * Method to inform that a pen has been connected
 *
 * @param penType ActivePenType the active pen type that has been connected.
 */
- (void) activePenDidConnect:(ActivePenType)penType;

/**
 * Method to inform that a pen has been disconnected
 *
 * @param penType ActivePenType the active pen type that has been disconnected.
 */
- (void) activePenDidDisconnect:(ActivePenType)penType;

/**
 * Method to inform that a pen is not supported. This could happen if the device itself does not support the active pen you've specified in the `activePenType` property
 *
 * @param penType ActivePenType the active pen type
 */
- (void) activePenNotSupported:(ActivePenType)penType;

@end


/**
 * Protocol used to return the user's signature
 */
@protocol SignatureAcquisitionViewDelegate <NSObject>

@optional

/**
 * Method to receive the user's signature.
 *
 * @param signature UIImage of the user's signature (use the customization options if you want to change the appareance of this image).
 * @param rect CGRect with the information of the signature size and position within the acquisition view.
 * @param data SignatureDataSet Object with the actual information of the user's signature
 */
- (void) obtainAcquisedSignature:(UIImage *)signature inRect:(CGRect)rect data:(SignatureDataSet*)data;

@end

/**
 The `SignatureAcquisitionView` handles the acquisition of the user's handwritten signature.
 
 - Use the `SignatureAcquisitionViewDelegate` to receive the signature once the user has signed on the device's screen
 - Use the `ActivePenManagerDelegate` if you want to get information about the use of the active pen of your choice (when the SDK it's searching for it, connecting and so on).
 
 ## Customization options ##
 
 This view lets you customize some aspects of its UI. To do so you can these customization properties:
 
 - `strokeColor` It lets you change the color of the signature.
 - `strokeWidth` It lets you change the width of the stroke of the signature.
 - `background` It lets you change the background of the view.
 
 */
@interface SignatureAcquisitionView : UIView

/**
 * property of the SignatureAcquisitionViewDelegate.
 */
@property (nonatomic, weak) id <SignatureAcquisitionViewDelegate> delegate;

/**
 * property of the ActivePenManagerDelegate.
 */
@property (nonatomic, weak) id <ActivePenManagerDelegate> activePenDelegate;

/**
 * property to enable or disable the active pen mode.
 */
@property (nonatomic) BOOL activePen;

/**
 * property to select which of the active pen supported by the SDK should be used
 */
@property (nonatomic) ActivePenType activePenType;

#pragma mark -- Customization View's properties

/**
 * property to customize the signature stroke color.
 */
@property (nonatomic, strong) UIColor * strokeColor;

/**
 * property to customize the signature stroke width. The values must be between 0 and 10.
 */
@property (nonatomic, strong) NSNumber * strokeWidth;

/**
 * property to customize the background of the view. It could be a UIColor object or a UIImage.
 */
@property (nonatomic, strong) id background;

/**
 * Method to start the signature acquisition process.
 */
- (void) startSignatureAcquisition;

/**
 * Method to stop the signature acquisition process.
 */
- (void) stopSignatureAcquisition;

/**
 * Method to redraw the signature with a given scale.
 *
 * @param scale CGFloat value to used as an scale to redraw the signature.
 *
 * @return UIImage with the user's signature scaled using the `scale` parameter.
 */
- (UIImage*) redrawSignatureWithScale:(CGFloat)scale;

@end
