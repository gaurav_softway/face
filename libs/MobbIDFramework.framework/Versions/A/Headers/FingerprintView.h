//
//  SignatureView.h
//  MobbIDFramework
//
//  Created by Raul Jareño diaz on 09/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "MobbIDSDKProgressUpdateDelegate.h"

/**
 FingerprintView is the Biometric Recognition View that should be used to enroll (register) or verify a user using his/her fingerprint as the biometric method.
 
 Before performing any biometric operation (enrollment or verification) please ensure that:
 
 - The user exists (@see MobbIDManagementAPI for more information)
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the FingerprintView to enroll an existing user you must follow these steps:
 
 1) Create and init FingerprintView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `FingerprintView *fingerprintView = [[FingerprintView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[fingerprintView setDelegate:self];`
 
 3) Add the FingerprintView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:fingerprintView];`
 
 4) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[fingerprintView startRecognitionIn:MobbIDSDKRecognitionMode_Enrollment forUser:USERID];`
 
 5) Stop the biometric recognition process when it is finished (method enrollmentFinishedForUser:result:method: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[fingerprintView stopRecognition];`
 
 To use the FingerprintView to verify a previously enrolled user you follow the previous steps and specify the verification mode instead of the enrollment:
 
 `// fingerprintView is init and added as a subview to the ViewController's view.`
 
 `[fingerprintView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 
 #Customization Options#
 
 Refer to the addCustomizationProperty:forKey:error: method for more information about the level of customization provided by this view.
 
 */
@interface FingerprintView : MobbIDSDKBaseView

@end
