//
//
//  Created by Abraham Holgado García on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import "MobbIDBaseAPI.h"

#import "StartEnrollmentDelegate.h"
#import "EnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"
#import "StartIdentificationDelegate.h"
#import "IdentificationDelegate.h"


/** 
 MobbID FaceAPI provides methods to enroll, verify and idenfity users using their faces as the biometric feature.
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 To be able to verify the identity of the users (or identify them) it is mandatory to enroll them previously on the system.
 
 Each biometric process (enrollment, verification and identification) must be initiated with its corresponding "start" method before calling the doEnrollmentOfUser:andData:delegate:, doVerificationOfUser:withType:forTransactionId:andData:attemptType:delegate: ...
 
 */
@interface FaceAPI : MobbIDBaseAPI

/** 
 Enrolls an existing user with an image previously acquired of the user's face.
 
 @param userId The user to be enrolled.
 @param data The UIImage objects containing the face of the user.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doEnrollmentOfUser:(NSString*)userId
                   andData:(UIImage*)data
                  delegate:(id <EnrollmentDelegate>)delegate;

/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param data The UIImage objects containing the face
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use the MobbIDAPIVerificationSampleAttempt_Unknown.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(UIImage*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;


/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage objects containing the face
 @param type MobbIDAPIVerificationSampleAttempt. Ignore this value. It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Use the MobbIDAPIVerificationSampleAttempt_Unknown.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(UIImage*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                    delegate:(id <VerificationDelegate>)delegate;


/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param data The UIImage objects containing the face
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(UIImage*)data
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage objects containing the face
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(UIImage*)data
                    delegate:(id <VerificationDelegate>)delegate;



/**
 Initiates the identification process for the given user.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startIdentificationdelegate:(id <StartIdentificationDelegate>)delegate;

/**
 Try to identify an user using the face.
 
 @param data The UIImage objects representing a face previously acquired.
 @param delegate The delegate to be informed with the result of the operation.
 
 @since 3.4
 */
- (void)doIdentificationWithData:(UIImage*)data
                        delegate:(id <IdentificationDelegate>)delegate;

/** 
 Try to identify an user using the face. 
 
 @param identificationType The action to perform when the identification is successful.The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of identification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the identification is successful. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The UIImage objects representing a face previously acquired.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doIdentificationWithType:(MobbIDAPIVerificationType)identificationType
                forTransactionId:(NSString*)transactionId
                         andData:(UIImage*)data
                        delegate:(id <IdentificationDelegate>)delegate;

@end
