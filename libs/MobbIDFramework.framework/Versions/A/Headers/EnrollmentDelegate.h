//
//  EnrollmentDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 12/04/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the biometricAPI (FaceAPI, VoiceAPI, SignatureAPI...) doEnrollmentOfUser:andData:delegate: operation.
 */
typedef NS_ENUM(NSInteger, MobbIDAPIEnrollmentDelegateResult)  {
    /** User has been enrolled properly on the system. */
    MobbIDAPIEnrollmentDelegateResult_OK,
    /** The system needs more samples to complete the operation */
    MobbIDAPIEnrollmentDelegateResult_NEED_MORE_SAMPLES,
    /** There has been some error with the operation */
    MobbIDAPIEnrollmentDelegateResult_ERROR
};

/**
 Data returned by the enrollment process.
 
 @since 3.4
 */
@interface MobbIDAPIEnrollmentResultData : NSObject

/** The user identification of the user that is being enrolled. */
@property (nonatomic, copy) NSString * userId;

/** Value from 0.0 to 100.0 that determines the progress to obtain a successful result. */
@property (nonatomic, assign) CGFloat progressOK;

/** Value from 0.0 to 100.0 that determines the progress to obtain an erroneous  result. */
@property (nonatomic, assign) CGFloat progressKO;

/** The token returned by the operation. */
@property (nonatomic, copy) NSString * token;

/** The synchronization token returned by the operation. This token might be use later to retrieve the biometric template of the user and synchronize the online/offline modes */
@property (nonatomic, copy) NSString * synchronizationToken;

/** The biometric key of the user generated during the enrollment process. Its value dependens on the server configuration (Ask the support team for more information) */
@property (nonatomic, copy) NSString * key;

@end



/**
 Delegate to be informed of the result of a biometric enrollment process. It is used for these biometric APIs: FaceAPI, VoiceAPI, IrisAPI, SignatureAPI.
 
 Once the biometric enrollment process has started, the API would always return (asynchronously) a value for every time the "doEnrollmentOfUser..." method is invoked. Since the number of biometric samples is not fixed, the actual numbers of invocations is unknown. The usual process could be something like this:
 
 1. Start enrollment process (startEnrollment:delegate:)
 2. Invoke doEnrollmentOfUser with the acquired sample(s)
 3. Receive the result with this delegate. If the result is MobbIDAPIEnrollmentDelegateResult_OK the process has finished. If the result is MobbIDAPIEnrollmentDelegateResult_NEED_MORE_SAMPLES repeat step #2 (keep repeating until the process has finished or an error has been received).
 
 */
@protocol EnrollmentDelegate <NSObject>

/**
 Sent to the delegate when the enrollment operation has finished.
 
 @param result The result of the enrollment process.
 @param data The data returned by the enrollment operation.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_ENROLL The enrollment process has not been started with the corresponding "startEnrollment" operation.
 - MobbIDAPIErrorCode.ERROR_USER_ALREADY_ENROLLED_IN_METHOD The user is already enrolled on this method
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 
 @warning Always use the constans from MobbIDBaseAPI, their actual values could change from one version to another.
 
 @discussion In the next mayor release (4.0) this will be the only method available for this delegate.
 
 */
- (void)enrollmentFinished:(MobbIDAPIEnrollmentDelegateResult)result
                      data:(MobbIDAPIEnrollmentResultData *)data
                     error:(NSError *)errorOccurred;

/**
 Sent to the delegate when the enrollment operation has finished.
 
 @param result The result of the enrollment process.
 @param userId The userId of the user that is being enrolled.
 @param progressOK value from 0.0 to 100.0 that determines the progress to get a correct process.
 @param progressKO value from 0.0 to 100.0 that determines the progress to get a failed process.
 @param token The token used to syncronize the biometric template enrolled
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_ENROLL The enrollment process has not been started with the corresponding "startEnrollment" operation.
 - MobbIDAPIErrorCode.ERROR_USER_ALREADY_ENROLLED_IN_METHOD The user is already enrolled on this method
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 
 @warning Always use the constans from MobbIDBaseAPI, their actual values could change from one version to another.
 
 @deprecated This method will be remove from the API in the next mayor release (4.0). Please use enrollmentFinished:data:error instead.
 */

@optional
- (void)enrollmentFinished:(MobbIDAPIEnrollmentDelegateResult)result
                      user:(NSString *)userId
                progressOK:(float)progressOK
                progressKO:(float)progressKO
                     token:(NSString *)token
                     error:(NSError *)errorOccurred;

@end
