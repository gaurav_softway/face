//
//  VerificationDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 12/04/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the biometricAPI (FaceAPI, VoiceAPI, SignatureAPI...) doVerificationOfUser:withType:forTransactionId:andData:attemptType:delegate:
 */
typedef NS_ENUM(NSInteger, MobbIDAPIVerificationDelegateResult) {
    /** The user has been verified */
    MobbIDAPIVerificationDelegateResult_USER_VERIFIED,
    /** The user has not been verified */
    MobbIDAPIVerificationDelegateResult_USER_NOT_VERIFIED,
    /** The system needs more samples to complete the operation */
    MobbIDAPIVerificationDelegateResult_NEED_MORE_SAMPLES,
    /** There has been some error with the operation */
    MobbIDAPIVerificationDelegateResult_ERROR
};

/**
 Data returned by the verification process.
 
 @since 3.4
 */
@interface MobbIDAPIVerificationResultData : NSObject

/** The user identification of the user that is being verified. */
@property (nonatomic, copy) NSString * userId;

/** 
 This value will return the biometric method in which the user is trying to be verified. It could take one of this values (the constants are defined in MobbIDBaseAPI):
 
 - kMobbIDAPI_BiometricMethod_Signature;
 - kMobbIDAPI_BiometricMethod_Iris;
 - kMobbIDAPI_BiometricMethod_Face;
 - kMobbIDAPI_BiometricMethod_Voice;
 - kMobbIDAPI_BiometricMethod_VoiceFace;
 
 */
@property (nonatomic, copy) NSString * biometricMethod;

/** Value from 0.0 to 100.0 that determines the progress to obtain a successful result. */
@property (nonatomic, assign) CGFloat progressOK;

/** Value from 0.0 to 100.0 that determines the progress to obtain an erroneous  result. */
@property (nonatomic, assign) CGFloat progressKO;

/** The token returned by the operation. */
@property (nonatomic, copy) NSString * token;

/** 
 The synchronization token returned by the operation. This token might be use later to retrieve the biometric template of the user and synchronize the online/offline modes 
 */
@property (nonatomic, copy) NSString * synchronizationToken;

/** 
 The biometric key of the user re-generated during the verification process. Its value dependens on the server configuration (Ask the support team for more information) 
 */
@property (nonatomic, copy) NSString * key;

@end

/**
 Delegate to be informed with the result of a biometric verification process. It is used for all the biometric APIs: FaceAPI, VoiceAPI, IrisAPI, SignatureAPI and FaceVoiceAPI.
 
 Once the biometric verification process has started, the API would always return (asynchronously) a value for every time the "doVerificationOfUser..." is invoked. Since the number of biometric samples is not fixed, the actual numbers of invocations is unknown. The usual process could be something like this:
 
 1. Start verification process (startVerification:delegate:)
 2. Invoke doVerificationOfUser with the acquired sample(s)
 3. Receive the result with this delegate. If the result is MobbIDAPIVerificationDelegateResult_USER_VERIFIED or MobbIDAPIVerificationDelegateResult_USER_NOT_VERIFIED the process has finished. If the result is MobbIDAPIVerificationDelegateResult_NEED_MORE_SAMPLES repeat step #2 (keep repeating until the process has finished or an error has been received).
 
 */
@protocol VerificationDelegate <NSObject>

/**
 Sent to the delegate when the verification operation has finished.
 
 @param result The result of the verification process.
 @param data The data returned by the enrollment operation.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_WRONG_PARAMETERS if there is a required input parameter that is missing or that it has a wrong format.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ENROLLED_ON_THIS_METHOD The user is not enrolled in this biometric method.
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_VERIFY The verification process has not been started with the corresponding "startVerification" operation.
 - MobbIDAPIErrorCode.ERROR_METHOD_NOT_SYNCHRONIZED The verification process cannot start in offline mode because the enrollment was done in online mode and the synchronization mode was disabled or because the synchronization process have failed

 @discussion In the next mayor release (4.0) this will be the only method available for this delegate.
 
 */
- (void)verificationFinished:(MobbIDAPIVerificationDelegateResult)result
                        data:(MobbIDAPIVerificationResultData *)data
                       error:(NSError *)errorOccurred;

@optional

/**
 Sent to the delegate when the verification operation has finished.
 
 @param result The result of the verification process.
 @param userId The userId of the user that was being verified.
 @param progressOK value from 0.0 to 100.0 that determines the progress to get a correct process.
 @param progressKO value from 0.0 to 100.0 that determines the progress to get a failed process. 
 @param biometricMethod NSString This value will return the biometric method in which the user is trying to be verified. It could take one of this values (the constants are defined in MobbIDBaseAPI):
 
- kMobbIDAPI_BiometricMethod_Signature;
- kMobbIDAPI_BiometricMethod_Iris;
- kMobbIDAPI_BiometricMethod_Face;
- kMobbIDAPI_BiometricMethod_Voice;
- kMobbIDAPI_BiometricMethod_VoiceFace;

 @param token TODO (Document it!)
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_WRONG_PARAMETERS if there is a required input parameter that is missing or that it has a wrong format.
 - MobbIDAPIErrorCode.ERROR_ON_SAMPLES An error has occurred working with the received samples, probably the samples are not good enough to perform the operation.
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ENROLLED_ON_THIS_METHOD The user is not enrolled in this biometric method.
 - MobbIDAPIErrorCode.WARNING_DUPLICATED_SAMPLES The sample(s) has been already received in a previous request. The samples are duplicated!
 - MobbIDAPIErrorCode.ERROR_USER_NOT_ALLOWED_TO_VERIFY The verification process has not been started with the corresponding "startVerification" operation.
 - MobbIDAPIErrorCode.ERROR_METHOD_NOT_SYNCHRONIZED The verification process cannot start in offline mode because the enrollment was done in online mode and the synchronization mode was disabled or because the synchronization process have failed

 @deprecated This method will be remove from the API in the next mayor release (4.0). Please use enrollmentFinished:data:error instead.
 */
- (void)verificationFinished:(MobbIDAPIVerificationDelegateResult)result
                        user:(NSString *)userId
                  progressOK:(float)progressOK
                  progressKO:(float)progressKO
             biometricMethod:(NSString *)biometricMethod
                       token:(NSString *)token
                       error:(NSError *)errorOccurred;

@end
