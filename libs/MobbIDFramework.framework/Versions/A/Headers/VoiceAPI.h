//
//
//  Created by Abraham Holgado García on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MobbIDBaseAPI.h"

#import "StartEnrollmentDelegate.h"
#import "EnrollmentDelegate.h"
#import "StartVerificationDelegate.h"
#import "VerificationDelegate.h"

/**
 This enum is used to indicate the type of voice enrollment.
 
 - MobbIDAPIVoiceAPI_EnrollmentType_RandomNumbers: The user would repeat random numbers an unfixed number of times.
 - MobbIDAPIVoiceAPI_EnrollmentType_Passphrase: The user would repeat the same passphrase an unfixed number of times.
 - MobbIDAPIVoiceAPI_EnrollmentType_FreeSpeech: The user would be able to say whatever he/she wants an unfixed number of times.
 
 */
typedef enum _MobbIDAPIVoiceAPI_EnrollmentType {
    MobbIDAPIVoiceAPI_EnrollmentType_RandomNumbers = 0,
    MobbIDAPIVoiceAPI_EnrollmentType_Passphrase = 1,
    MobbIDAPIVoiceAPI_EnrollmentType_FreeSpeech = 2
} MobbIDAPIVoiceAPI_EnrollmentType;

/**
 MobbID VoiceAPI provides methods to enroll and verify users using their voices as the biometric feature.
 
 Every operation is performed asynchronously and has its own delegate to inform of the result.
 
 To be able to verify the identity of the users it is mandatory to enroll them previously on the system.
 
 Each biometric process (enrollment and verification) must be initiated with its corresponding "start" method before calling the "doEnrollmentOfUser..." or "doVerificationOfUser:..."
 
 @warning *Important:*Online vs Standlone (offline) mode.
 
 Currently the MobbIDAPI could work in both modes **but** some biometric methods are not supported in offline mode.
 
 Voice could work in both modes but there are some differences that must be taken into accout:
 
 - Standlone mode:
    - Only MobbIDAPIVoiceAPI_EnrollmentType_Passphrase is available in the current release.
 
 MobbIDAPIVoiceAPI_EnrollmentType_FreeSpeech is currently disabled for both modes (Online and Offline).
 */
@interface VoiceAPI : MobbIDBaseAPI

/**
 Initiates the enrollment process for the given user.
 
 @param userId The userId to be enrolled.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startEnrollment:(NSString *)userId delegate:(id <StartEnrollmentDelegate>)delegate;

/**
 Enrolls an existing user with an image previously acquired of the user's voice.
 
 @param userId The user to be enrolled.
 @param data The NSData object representing a voice previously acquired.
 @param flac YES if the data is compressed using flac format.
 @param type Indicates the type of voice enrollment the user is performing. The possible values are:
 
 - MobbIDAPIVoiceAPI_EnrollmentType_RandomNumbers
 - MobbIDAPIVoiceAPI_EnrollmentType_Passphrase
 - MobbIDAPIVoiceAPI_EnrollmentType_FreeSpeech
 
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doEnrollmentOfUser:(NSString*)userId 
                   andData:(NSData*)data
                flacFormat:(BOOL)flac
            enrollmentType:(MobbIDAPIVoiceAPI_EnrollmentType)type
                  delegate:(id <EnrollmentDelegate>)delegate;

/**
 Initiates the verification process for the given user.
 
 @param userId The user to be verified.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)startVerification:(NSString*)userId delegate:(id <StartVerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The NSData object representing a voice previously acquired.
 @param type It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Ignore this parameter.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId 
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(NSData*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                  flacFormat:(BOOL)flac
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data The NSData object representing a voice previously acquired.
 @param type It is only for debug purposes. It indicates if the user trying to verify is the genuine user or it is a forgery attempt. Ignore this parameter.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(NSData*)data
                 attemptType:(MobbIDAPIVerificationSampleAttempt)type
                  flacFormat:(BOOL)flac
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param verificationType The action to perform when the verification is successful. The possible values are:
 
 - MobbIDAPIVerificationType_Simple: No further action.
 - MobbIDAPIVerificationType_Login: Log the user into the system.
 - MobbIDAPIVerificationType_Transaction: Log the user into the system for a specific operation. When using this type of verification, transactionId parameter cannot be nil.
 
 @param transactionId The unique identifer of an specific transaction to be done if the verification is positive. Unless the verificationType is MobbIDAPIVerificationType_Transaction, this value will be ignored.
 @param data The NSData object representing a voice previously acquired.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 
 @deprecated In a next mayor release, this method would be removed.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withType:(MobbIDAPIVerificationType)verificationType
            forTransactionId:(NSString*)transactionId
                     andData:(NSData*)data
                  flacFormat:(BOOL)flac
                    delegate:(id <VerificationDelegate>)delegate;

/**
 Try to verify the identity of an existing user using his voice.
 
 @param userId The user to be verified.
 @param data The NSData object representing a voice previously acquired.
 @param flac YES if the data is compressed using flac format.
 @param delegate The delegate to be informed with the result of the operation.
 */
- (void)doVerificationOfUser:(NSString*)userId
                    withData:(NSData*)data
                  flacFormat:(BOOL)flac
                    delegate:(id <VerificationDelegate>)delegate;

- (NSArray *) generateTextNumbersForLanguage:(MobbIDAPISupportedLanguage)language;

- (NSArray *) generateTextForLanguage:(MobbIDAPISupportedLanguage)language;

@end
