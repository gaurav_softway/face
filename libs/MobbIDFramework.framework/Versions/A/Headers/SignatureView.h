//
//  SignatureView.h
//  MobbIDFramework
//
//  Created by Raul Jareño diaz on 09/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "SignatureAcquisitionView.h"
#import "MobbIDSDKProgressUpdateDelegate.h"

/**
 The delegate of a Signature capture sample View (SignatureView) must adopt this protocol to receive the samples of the biometric view. It receives an array of SignatureDataSet
 */
@protocol SignatureCaptureSampleDelegate <NSObject>

@required

/**
 This method will be called every time a signature is capture.
 @param signatures NSArray with SignatureDataSet objects.
 */
- (void)signatureSamplesCaptured:(NSArray *)signatures;

@end

/**
 SignatureView is the Biometric Recognition View that should be used to enroll (register) or verify a user using his/her handwritten signature as the biometric method.
 
 Before performing any biometric operation (enrollment or verification) please ensure that:
 
 - The user exists (@see MobbIDManagementAPI for more information)
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the SignatureView to enroll an existing user you must follow these steps:
 
 1) Create and init SignatureView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `SignatureView *signatureView = [[SignatureView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[signatureView setDelegate:self];`
 
 3) Add the SignatureView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:signatureView];`
 
 4) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[signatureView startRecognitionIn:MobbIDSDKRecognitionMode_Enrollment forUser:USERID];`
 
 5) Stop the biometric recognition process when it is finished (method enrollmentFinishedForUser:result:method: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[signatureView stopRecognition];`
 
 To use the SignatureView to verify a previously enrolled user you follow the previous steps and specify the verification mode instead of the enrollmet:
 
 `// signatureView is init and added as a subview to the ViewController's view.`
 
 `[signatureView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 To use the SignatureView to capture the samples you have to set up its captureDelegate, conforming the SignatureCaptureSampleDelegate. The captureDelegate does not have to be the ViewController (it could be any class conforming the SignatureCaptureSampleDelegate).
 
 `[signatureView setCaptureDelegate:self];`
 
 If you dont want to perform the recognition you have to use the MobbIDSDKRecognitionMode_Capture in the method startRecognitionIn:forUser:
 
 `[signatureView startRecognitionIn:MobbIDSDKRecognitionMode_Capture forUser:USERID];`
 
 
 
 #Customization Options#
 
 Refer to the addCustomizationProperty:forKey:error: method for more information about the level of customization provided by this view.
 
 */
@interface SignatureView : MobbIDSDKBaseView

/**
 Set this delegate if you want to get the sample information of each signature captured.
 */
@property (nonatomic, weak) id <SignatureCaptureSampleDelegate> captureDelegate;

/**
 Set this delegate if you want to get informed when an active pen is discovered, connected, disconnected, etc.
 */
@property (nonatomic, weak) id <ActivePenManagerDelegate> activePenDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the recognition progress.
 */
@property (nonatomic, weak) id <MobbIDSDKProgressUpdateDelegate> progressUpdateDelegate;

/**
 YES if an active pen must be used when capturing the user's signature. NO Otherwire
 */
@property (nonatomic) BOOL activePen;

/**
 Indicates which supported active pen is going to be used. This value will be take into account if the activePen property's value is YES.
 */
@property (nonatomic, assign) ActivePenType activePenType;

/**
 Adds a new customization property, overriding the default value. Current available properties are:
 
 - @"signature.background.image" -> (UIImage*) - background of the signature layout. Set nil value if you don't want the default grid background.
 - @"signature.background.color" -> (UIColor*) - background color of the signature layout.
 - @"signature.stroke.color" -> (UIColor*) - color of the signature stroke.
 - @"signature.stroke.width" -> (NSNumber*) - width of the signature stroke (from 1 to 10).
 - @"signature.capture.borderColor" -> (UIColor*) - color of the border of the capture rectangle over the signature.
 - @"signature.capture.fillColor" -> (UIColor*) - color of the capture rectangle over the signature.
 - @"signature.capture.dashedBorder" -> (NSNumber*) - BOOL value indicating in border line should be solid (NO) or dashed (YES).
 - @"signature.capture.borderWidth" -> (NSNumber*) - width of the border of the capture rectangle over the signature (from 1 to 10).
 
 @param value The actual property value to be set.
 @param key The key being override.
 @param error A NSError with information of the possible error (if key does not exist or the property set is not valid).
 
 @return YES if the customization property has been correctly updated. NO otherwise.
 
 @warning @"signature.background.image" and @"signature.background.color" are incompatible. @"signature.background.image" has the highest priority so if you set them both the background color will not be used.
 */
- (BOOL)addCustomizationProperty:(id)value forKey:(NSString *)key error:(NSError **)error;


@end
