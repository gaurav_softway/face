//
//  MobbIDUtils.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 12/04/13.
//
//

#import <Foundation/Foundation.h>
#import "MobbIDBaseAPI.h"

/**
 Utility class for others classes of the SDK.
 */
@interface MobbIDUtils : NSObject

/**
 Returns the complete path of the documents directory of the app that contains the SDK.
 
 @return NSString the generated UUID.
 */
+ (NSString *) applicationDocumentsDirectory;

/**
 Generates a Universally unique identifer (UUID).
 
 @return NSString the generated UUID.
 */
+ (NSString*) createUUID;

/**
 Returns the current clientVersion of the SDK.
 
 @return NSString the clientVersion.
 */
+ (NSString *) clientVersion;

/**
 Returns the current version of the SDK.
 
 @return NSString the version.
 */
+ (NSString *) sdkVersion;

/**
 Returns the requests timeout for the biometric server.
 
 @return NSUInteger the timeout of the server.
 */
+ (NSUInteger) httpTimeOut;

/**
 It will search for a bundle name "MobbIDAPI.bundle". If it's not found, it will try to load the "MobbIDFramework.bundle". If it's not found, then it will return the main bundle.
 
 @return apiBundle NSBundle with the necessary resources.
 */
+ (NSBundle *) apiBundle;

/**
 Return a description from the `MobbIDAPIVerificationSampleAttempt`
 
 @param attempt MobbIDAPIVerificationSampleAttempt
 
 @return NSString with a literal description of the `MobbIDAPIVerificationSampleAttempt`
 */
+ (NSString*)getVerificationSampleAttemptDescriptionFor:(MobbIDAPIVerificationSampleAttempt)attempt;

@end
